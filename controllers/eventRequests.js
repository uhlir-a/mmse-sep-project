const mongoose = require('mongoose');
const EventRequest = require('../models/EventRequest');
const Client = require('../models/Client');
const Event = require('../models/Event');
const Task = require('../models/Task');
const PreferencesEnum = require('../enums/PreferencesEnum');
const RedirectError = require('../tools/RedirectError');
const Error404 = require('../tools/Error404');

const validateForm = req => {
    "use strict";
    req.checkBody('eventType', 'Event type cannot be empty!').notEmpty();
    req.checkBody('dateFrom', '`Date from` cannot be empty!').notEmpty();
    req.checkBody('dateFrom', '`Date from` "%0" is not a date! It has to have format YYYY-MM-DD.').isDate();
    req.checkBody('dateFrom', '`Date from` "%0" is not a date in future!').isDate().isFutureDate();
    req.checkBody('dateTo', '`Date to` cannot be empty!').notEmpty();
    req.checkBody('dateTo', '`Date to` "%0" is not a date! It has to have format YYYY-MM-DD.').isDate();
    req.checkBody('dateTo', '`Date to` "%0" is not a date in future!').isDate().isFutureDate();
    req.checkBody('dateFrom', '`Date to` has to be further then `Data from`').isDateFurtherOrEqual(req.body.dateTo);
    req.checkBody('numberOfAttendees', 'Number of attendees cannot be empty and has to be bigger than 0!').notEmpty().isPositiveInt();
    req.checkBody('budget', 'Budget "%0" has to be not empty and has to be a positive number!').notEmpty().isPositiveNumeric();
    req.checkBody('preferences', 'Preferences has to be an array of string!').isArray();
    req.checkBody('client', 'Client has to be an valid Id!').isMongoId();
    req.sanitizeBody('client').toMongoId();

    return req.validationErrors();
};
exports.validateForm = validateForm;

const isIdValid = (req, res, redirectUrl = '/event-requests') => {
    req.checkParams('eventRequestId', 'Event Request Id has to be set and be valid Id!').isMongoId();
    req.sanitizeParams('eventRequestId').toMongoId();
    let errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect(redirectUrl);

        return false;
    }

    return true;
};
exports.isIdValid = isIdValid;

exports.getCreate = (req, res, errorHandling) => {
    "use strict";

    return Client.find().exec()
        .then(clients => {
            res.render('eventRequests/create.nunj', {
                clients: clients,
                preferences: PreferencesEnum._toArray()
            });
        }).catch(errorHandling);
};

exports.postCreate = (req, res, errorHandling) => {
    "use strict";
    const errors = validateForm(req);

    if (errors) {
        req.flash('errors', errors);
        return Client.find().exec()
            .then(clients => {
                res.render('eventRequests/create.nunj', {
                    clients: clients,
                    preferences: PreferencesEnum._toArray(),
                    data: req.body
                });
            }).catch(errorHandling);
    }

    // Parse ObjectId
    let data = req.body;
    data['createdBy'] = req.user._id;

    let eventRequest = new EventRequest(data);
    return eventRequest.save()
        .then(() => {
            req.flash('success', {msg: 'Event Request successfully created!'});
            res.redirect('/event-requests');
        })
        .catch(errorHandling);
};

exports.getList = (req, res, errorHandler) => {
    "use strict";

    const displayRejected = req.query.rejected !== undefined && req.query.rejected;

    return EventRequest.find({rejected: displayRejected}).populate('client').exec()
        .then(requests => {
            res.render('eventRequests/list.nunj', {
                eventRequests: requests,
                displayRejected: displayRejected
            });
        }).catch(errorHandler);
};

exports.getDetail = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    return EventRequest.findOne({_id: req.params.eventRequestId}).populate('client reviews.user createdBy').exec()
        .then(eventRequest => {
            if (!eventRequest) {
                throw new Error404();
            }

            res.render('eventRequests/detail.nunj', {
                request: eventRequest,
                wroteReview: eventRequest.userHaveReviewed(req.user)
            })
        }).catch(errorHandler);
};

exports.getUpdate = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    let request = EventRequest.findOne({_id:req.params.eventRequestId}).exec();
    let clients = Client.find().exec();

    return Promise.all([request, clients])
        .then(([eventRequest, clients]) => {
            if (!eventRequest) {
                throw new Error404();
            }

            if (eventRequest.rejected) {
                req.flash('error', {msg: 'The requested Event Request was already rejected, so it can not be edited anymore!'});
                throw new RedirectError('/event-requests');
            }

            res.render('eventRequests/update.nunj', {
                data: eventRequest,
                clients: clients,
                preferences: PreferencesEnum._toArray()
            });
        }).catch(errorHandler);
};

exports.postUpdate = (req, res, errorHandler) => {
    "use strict";
    req.checkParams('eventRequestId', 'Event Request Id has to be set and be valid Id!').isMongoId();
    req.sanitizeParams('eventRequestId').toMongoId();
    const errors = validateForm(req);

    if (errors) {
        req.flash('errors', errors);
        return Client.find().exec()
            .then(clients => {
                res.render('eventRequests/update.nunj', {
                    clients: clients,
                    preferences: PreferencesEnum._toArray(),
                    data: req.body
                });
            }).catch(errorHandler);
    }

    return EventRequest.update({_id: req.params.eventRequestId}, req.body)
        .then(() => {
            req.flash('success', {msg: 'Event Request successfully updated!'});
            res.redirect('/event-requests');
        })
        .catch(errorHandler);
};

exports.getReview = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    return EventRequest.findOne({_id: req.params.eventRequestId}).exec()
        .then(eventRequest => {
            if (!eventRequest) {
                throw new Error404();
            }

            if (eventRequest.rejected) {
                req.flash('error', {msg: 'The Event Requests you wanted to write an review, was already rejected!!'});
                throw new RedirectError('/event-requests/' + req.params.eventRequestId);
            }

            if (eventRequest.reviews.filter(review => {return review.user.equals(req.user._id)}).length > 0) {
                req.flash('error', {msg: 'You have already written your review!'});
                throw new RedirectError('/event-requests/' + req.params.eventRequestId);
            }

            res.render('eventRequests/review.nunj');
        }).catch(errorHandler);
};

exports.postReview = (req, res, errorHandler) => {
    "use strict";
    req.checkParams('eventRequestId', 'Event Request Id has to be set and be valid Id!').isMongoId();
    req.sanitizeParams('eventRequestId').toMongoId();
    req.checkBody('feedback', 'Feedback has to be filled!').notEmpty();
    req.checkBody('type', 'Unknown error! (Submit type not set)').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        return res.render('eventRequests/review.nunj');
    }

    return EventRequest.findOne({_id: req.params.eventRequestId}).exec()
        .then(eventRequest => {
            if (!eventRequest) {
                throw new Error404();
            }

            eventRequest.reviews.push({
                user: req.user._id,
                feedback: req.body.feedback
            });

            // TODO: Check if user has proper role for this
            if(req.body.type == 'reject'){
                eventRequest.rejected = true;
            }

            return eventRequest.save()
        })
        .then(() => {
            req.flash('success', {msg: 'Review successfully created!'});
            res.redirect('/event-requests/' + req.params.eventRequestId);
        }).catch(errorHandler);
};

exports.getReject = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    return EventRequest.findOne({_id: req.params.eventRequestId}).exec()
        .then(request => {
            if (!request) {
                throw new Error404();
            }

            request.rejected = true;

            return request.save()
        })
        .then(() => {
            req.flash('success', {msg: 'Event Request was successfully rejected!'});
            res.redirect('/event-requests');
        }).catch(errorHandler);
};

exports.getAccept = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    return EventRequest.findOne({_id: req.params.eventRequestId}).exec()
        .then(eventRequest => {
            if (!eventRequest) {
                throw new Error404();
            }

            if(eventRequest.rejected){
                req.flash('error', {msg: 'You want to accept Event Request which was already rejected, which is not possible!'});
                throw new RedirectError('/event-requests');
            }

            const event = new Event({
                eventType: eventRequest.eventType,
                dateFrom: eventRequest.dateFrom,
                dateTo: eventRequest.dateTo,
                numberOfAttendees: eventRequest.numberOfAttendees,
                budget: eventRequest.budget,
                client: eventRequest.client,
                approvedBy: req.user._id
            });

            let tasks = [], tasksPromise = [];
            for(let preference of eventRequest.preferences){
                let task = new Task({name: preference, event: event._id});
                tasks.push(task._id);
                tasksPromise.push(task.save());
            }
            
            // Other Task
            let task = new Task({name: 'other', event: event._id});
            tasks.push(task._id);
            tasksPromise.push(task.save());

            event.tasks = tasks;

            return Promise.all([event.save(), eventRequest.remove(), ...tasksPromise])
        })
        .then(() => {
            req.flash('success', {msg: 'Event Request was successfully accepted!'});
            res.redirect('/event-requests');
        }).catch(errorHandler);
};
