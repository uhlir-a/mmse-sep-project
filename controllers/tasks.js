const Task = require('../models/Task');
const RedirectError = require('../tools/RedirectError');
const Error404 = require('../tools/Error404');

exports.getList = (req, res, errorHandler) => {
    "use strict";

    const displayCompleted = req.query.completed !== undefined && req.query.completed;

    return Task.find({assignedTo: req.user._id, completed: displayCompleted}).populate('event assignedBy').exec()
        .then(tasks => {
            res.render('tasks/list.nunj',{
                tasks: tasks,
                displayCompleted: displayCompleted
            })
        }).catch(errorHandler);
};


exports.getComplete = (req, res , errorHandler) => {
    "use strict";

    req.checkParams('taskId', 'Task Id has to be set and valid Id!').isMongoId();
    req.sanitizeParams('taskId').toMongoId();
    let errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        return res.redirect('/tasks'); // TODO: Redirect back from where he came (not everybody will have access to List of Events
    }

    return Task.findOne({_id: req.params.taskId}).exec()
        .then(task => {
            if(!task){
                throw new Error404();
            }

            if(!task.assignedTo.equals(req.user._id)){
                req.flash('error', {msg: 'You can not complete task, which is not your own!'});
                throw new RedirectError('/tasks');
            }

            task.completed = true;
            return task.save();
        })
        .then(() => {
            req.flash('success', {msg: 'Task successfully completed!'});
            res.redirect('/tasks');
        }).catch(errorHandler);
};