const ResourceRequest = require('../models/ResourceRequest');
const Task = require('../models/Task');
const Event = require('../models/Event');
const RolesEnum = require('../enums/RolesEnum');
const DepartmentEnum = require('../enums/DepartmentEnum');
const RedirectError = require('../tools/RedirectError');
const Error404 = require('../tools/Error404');

/**
 * Helper validator for Task ids that are passed in the url (not in the query string)
 */
function validateTaskId(req) {
	"use strict";
	req.checkParams('taskId', 'The given task id is not a valid id').isMongoId();
	req.sanitizeParams('taskId').toMongoId();
	return req.validationErrors();
}

/**
 * GET /resource-requests/:taskId/create
 * Creating a resource request page
 */
exports.getCreate = (req, res, next) => {
	const errors = validateTaskId(req);
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return Task.findOne({ _id: req.params.taskId }).exec().then(task => {
		if(!task){
			throw new Error404();
		}

        res.render('resourceRequests/create.nunj', {
            task: task,
            departments: DepartmentEnum
        });
	}).catch(next);
};

/**
 * POST /resource-requests/:taskId/create
 * Submitting the resource request data
 */
exports.postCreate = (req, res, next) => {
	req.checkBody('department', 'The department must be specified').notEmpty();
	req.checkBody('newBudget', 'The budget must be a positive number').isPositiveNumeric();
	req.checkBody('reason', 'The reason for the request cannot be empty').notEmpty();
	const errors = validateTaskId(req);
	if (errors) {
		req.flash('errors', errors);
		return Task.findOne({ _id: req.params.taskId }).exec().then(task => {
			if (!task) {
				throw new Error404();
			}

			res.render('resourceRequests/create.nunj', {
				data: req.body,
				task: task,
				departments: DepartmentEnum
			});
		}).catch(next);
		return;
	}

	return Task.findOne({ _id: req.params.taskId }).populate('event').exec().then(task => {
		if(!task){
			throw new Error404();
		}

		if (req.body.newBudget > task.event.budget) {
			req.flash('error', { msg: 'The requested new budget cannot be larger than the event budget.' });
			throw new RedirectError('/resource-requests');
		}

		new ResourceRequest({
			requester: req.user._id,
			department: req.body.department,
			newBudget: req.body.newBudget,
			reason: req.body.reason,
			task: task._id
		}).save().then(() => {
			req.flash('success', { msg: 'Resource request successfully submitted.' });
			res.redirect('/resource-requests');
		}).catch(next);
	}).catch(next);
};

/**
 * GET /resource-requests
 * Viewing resource requests page
 */
exports.getList = (req, res, next) => {

	const displayArchived = req.query.archived !== undefined && req.query.archived;

	let conditions = {};

	if (!displayArchived) {
		conditions.archived = false;
	}

	if (req.user.role == RolesEnum.PRODUCTION_MANAGER) {
		conditions.department = DepartmentEnum.PRODUCTION;
	} else if (req.user.role == RolesEnum.SERVICE_MANAGER) {
		conditions.department = DepartmentEnum.SERVICES;
	} else if (req.user.role == RolesEnum.TEAM_MEMBER) {
		conditions.requester = req.user._id;
	} else { // IF IT'S NOT PRODUCTION OR SERVICE OR TEAM_MEMBER THEN IT'S FINANCIAL MANAGER
		conditions.approvedByDptManager = true;
	}

	return ResourceRequest.find(conditions).populate({
		path: 'task',
		model: 'Task',
		populate: {
			path: 'event',
			model: 'Event'
		}
	}).exec().then(requests => {
		res.render('resourceRequests/list.nunj', {
			requests: requests,
			displayArchived: displayArchived
		});
	}).catch(next);
};

/**
 * GET /resource-requests/:requestId
 * Viewing a resource request's details page
 */
exports.getDetails = (req, res, next) => {
	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({ _id: req.params.requestId }).populate({
		path: 'task',
		model: 'Task',
		populate: {
			path: 'event',
			model: 'Event'
		}
	}).exec().then(request => {
		if(!request) {
			throw new Error404();
		}

		res.render('resourceRequests/details.nunj', { request: request });
	}).catch(next);
};

/**
 * GET /resource-requests/:requestId/approve
 * Approve a resource request as a department manager so that
 * the request will be "forwarded" to the financial manager
 */
exports.getApprove = (req, res, next) => {
	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This Resource request has already been archived and cannot be approved!'});
			throw new RedirectError('/resource-requests');
		} else if (request.approvedByDptManager) {
			req.flash('error', {msg: 'This Resource request has already been approved by the ' +
			request.department.charAt(0).toUpperCase() + request.department.substr(1) + ' Manager!'});
			throw new RedirectError('/resource-requests');
		}

		ResourceRequest.update({ _id: req.params.requestId }, {
			$set: { approvedByDptManager: true }
		}).then(() => {
			req.flash('success', { msg: 'Resource request successfully approved.' });
			return res.redirect('/resource-requests');
		}).catch(next);
	}).catch(next);
};

/**
 * GET /resource-requests/:requestId/accept
 * Accept a resource request as the financial manager
 */
exports.getAccept = (req, res, next) => {
	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This Resource request has already been archived and cannot be accepted!'});
			throw new RedirectError('/resource-requests');
		} else if (!request.approvedByDptManager) {
			req.flash('error', {msg: 'This Resource request has not yet been approved by the ' +
			request.department.charAt(0).toUpperCase() + request.department.substr(1) + ' Manager and cannot be accepted!'});
			throw new RedirectError('/resource-requests');
		}

		ResourceRequest.update({ _id: req.params.requestId }, {
			$set: { archived: true }
		}).then(() => {
			req.flash('success', { msg: 'Resource request successfully accepted.' });
			return res.redirect('/resource-requests');
		}).catch(next);
	}).catch(next);
};

/**
 * GET /resource-requests/:requestId/reject
 * Reject a resource request either as a department manager
 * or as the financial manager
 */
exports.getReject = (req, res, next) => {
	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This Resource request has already been archived and cannot be rejected!'});
			throw new RedirectError('/resource-requests');
		} else if (!request.approvedByDptManager && (req.user.role == RolesEnum.FINANCIAL_MANAGER)) {
			req.flash('error', {msg: 'This Resource request has not yet been approved by the ' +
			request.department.charAt(0).toUpperCase() + request.department.substr(1) + ' Manager and cannot be rejected!'});
			throw new RedirectError('/resource-requests');
		} else if (request.approvedByDptManager && (req.user.role != RolesEnum.FINANCIAL_MANAGER)) {
			let dpt = request.department.charAt(0).toUpperCase() + request.department.substr(1);
			req.flash('error', {msg: 'This Resource request has already been approved by the ' +
			dpt + ' Manager, so a ' + dpt + ' Manager cannot reject it at this point!'});
			throw new RedirectError('/resource-requests');
		}

		ResourceRequest.update({ _id: req.params.requestId }, {
			$set: {
				rejected: true,
				archived: true
			}
		}).then(() => {
			req.flash('success', { msg: 'Resource request successfully rejected.' });
			return res.redirect('/resource-requests');
		}).catch(next);
	}).catch(next);
};

/**
 * GET /resource-requests/:requestId/update
 * Updating a resource request page
 */
exports.getUpdate = (req, res, next) => {
	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		}

		if (request.archived) {
			req.flash('error', {msg: 'This Resource request has already been archived and cannot be updated!'});
			throw new RedirectError('/resource-requests');
		} else if (request.approvedByDptManager) {
			req.flash('error', {msg: 'This Resource request has already been approved by the ' +
			request.department.charAt(0).toUpperCase() + request.department.substr(1) + ' Manager and cannot be updated!'});
			throw new RedirectError('/resource-requests');
		}

		res.render('resourceRequests/create.nunj', {
			data: request,
			departments: DepartmentEnum
		});
	}).catch(next);
};

/**
 * POST /resource-requests/:requestId/update
 * Submitting the updated resource request data
 */
exports.postUpdate = (req, res, next) => {
	req.checkBody('department', 'The department must be specified').notEmpty();
	req.checkBody('newBudget', 'The budget must be a positive number').isPositiveNumeric();
	req.checkBody('reason', 'The reason for the request cannot be empty').notEmpty();

	req.checkParams('requestId', 'The given resource request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/resource-requests');
	}

	return ResourceRequest.findOne({_id: req.params.requestId}).populate({
		path: 'task',
		model: 'Task',
		populate: {
			path: 'event',
			model: 'Event'
		}
	}).exec()
		.then(request => {
			if (!request) {
				throw new Error404();
			}

			if (req.body.newBudget > request.task.event.budget) {
				req.flash('error', {msg: 'The requested new budget cannot be larger than the event budget.'});
				throw new RedirectError('/resource-requests');
			}

			return ResourceRequest.update({_id: req.params.requestId}, {
				$set: {
					department: req.body.department,
					newBudget: req.body.newBudget,
					reason: req.body.reason
				}
			});
		})
		.then(() => {
			req.flash('success', {msg: 'Resource request successfully updated.'});
			throw new RedirectError('/resource-requests');
		}).catch(next);
};
