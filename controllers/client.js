const Client = require('../models/Client');
const Event = require('../models/Event');
const mongoose = require('mongoose');
const Error404 = require('../tools/Error404');
const RedirectError = require('../tools/RedirectError');

/**
 * Helper validator for create and update
 */
function validate(req) {
	"use strict";
	req.checkBody('name', 'Name cannot be empty').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('phoneNumber', 'Phone cannot be empty').notEmpty();
	req.sanitize('email').normalizeEmail({ remove_dots: false });

	return req.validationErrors();
}
exports.validateForm = validate;

/**
 * GET /clients/create
 * Creating client page
 */
exports.getCreate = (req, res) => {
	res.render('clients/create.nunj');
};

/**
 * POST /clients/create
 * Submitting the client creation form data
 */
exports.postCreate = (req, res, next) => {
	const errors = validate(req);
	if (errors) {
		req.flash('errors', errors);
		return res.render('clients/create.nunj',{
			data: req.body
		});
	}
	
	const client = new Client(req.body);

	return Client.findOne({
		email: req.body.email
	}).exec().then(existingClient => {
		if (existingClient) {
			req.flash('errors', { msg: 'Client with that email address already exists.' });
			throw new RedirectError('/clients/create');
		}

		return client.save().then(() => {
			req.flash('success', { msg: 'Client successfully created.' });
			res.redirect('/clients');
		}).catch(next);
	}).catch(next);
};

/**
 * GET /clients
 * Client list page
 */
exports.getList = (req, res, next) => {
	return Client.find().exec().then(clients => {
		res.render('clients/list.nunj', { clients: clients });
	}).catch(next);
};

/**
 * GET /clients/:clientId/update
 * Updating client page
 */
exports.getUpdate = (req, res, next) => {
	req.checkParams('clientId', 'The given client id is not a valid id').isMongoId();
	req.sanitizeParams('clientId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/clients');
	}

	return Client.findOne({
		_id: req.params.clientId
	}).exec().then(client => {
		if (!client) {
			throw new Error404();
		} else {
			res.render('clients/create.nunj', { data: client });
		}
	}).catch(next);
};

/**
 * POST /clients/:clientId/update
 * Submitting the client update form data
 */
exports.postUpdate = (req, res, next) => {
	req.checkParams('clientId', 'The given client id is not a valid id').isMongoId();
	req.sanitizeParams('clientId').toMongoId();
	const errors = validate(req);
	if (errors) {
		req.flash('errors', errors);
		return res.render('clients/create.nunj',{
			data: req.body
		});
	}

	if(req.body.email){
		return Client.findOne({
			email: req.body.email
		}).exec().then(existingClient => {
			if (existingClient && !existingClient._id.equals(req.params.clientId)) {
				req.flash('errors', { msg: 'Client with that email address already exists.' });
				throw new RedirectError('/clients/create');
			}

			return Client.update({
				_id: req.params.clientId
			}, req.body)

		}).then(() => {
			req.flash('success', { msg: 'Client successfully updated.' });
			res.redirect('/clients');
		}).catch(next);
	}

	return Client.update({
		_id: req.params.clientId
	}, req.body).then(() => {
		req.flash('success', { msg: 'Client successfully updated.' });
		res.redirect('/clients');
	}).catch(next);
};

/**
 * GET /clients/:clientId
 * Client event history page
 */
exports.getEventHistory = (req, res, next) => {
	req.checkParams('clientId', 'The given client id is not a valid id').isMongoId();
	req.sanitizeParams('clientId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/clients');
	}

	let clientQuery = Client.findOne({
		_id: req.params.clientId
	}).exec();
	let eventsQuery = Event.find({
		client: mongoose.Types.ObjectId(req.params.clientId)
	}).exec();

	return Promise.all([clientQuery, eventsQuery]).then(results => {
		if (!results[0]) {
			throw new Error404();
		} else {
			res.render('clients/events.nunj', {
				client: results[0],
				events: results[1]
			});
		}
	}).catch(next);
};
