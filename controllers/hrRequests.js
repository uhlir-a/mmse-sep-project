const HrRequest = require('../models/HrRequest');
const Event = require('../models/Event');
const RolesEnum = require('../enums/RolesEnum');
const DepartmentEnum = require('../enums/DepartmentEnum');
const RedirectError = require('../tools/RedirectError');
const Error404 = require('../tools/Error404');

/**
 * Helper validator for Event ids that are passed in the url (not in the query string)
 */
function validateEventId(req) {
	"use strict";
	req.checkParams('eventId', 'The given event id is not a valid id').isMongoId();
	req.sanitizeParams('eventId').toMongoId();
	return req.validationErrors();
}
exports.isIdValid = validateEventId;

/**
 * GET /hr-requests/:eventId/create
 * Creating an HR request page
 */
exports.getCreate = (req, res, next) => {
	const errors = validateEventId(req);
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}

	return Event.findOne({ _id: req.params.eventId }).exec().then(event => {
		if(!event){
			throw new Error404();
		}

        res.render('hrRequests/create.nunj', {
            event: event,
            departments: DepartmentEnum
        });
	}).catch(next);
};

/**
 * POST /hr-requests/:eventId/create
 * Submitting the HR request data
 */
exports.postCreate = (req, res, next) => {
	req.checkBody('department', 'The department must be specified').notEmpty();
	req.checkBody('nbrOfEmployeesNeeded', 'The number of employees must be a positive integer').isPositiveInt();
	req.checkBody('reason', 'The reason for the request cannot be empty').notEmpty();
	const errors = validateEventId(req);
	if (errors) {
		req.flash('errors', errors);
		return Event.findOne({ _id: req.params.eventId }).exec().then(event => {
			if (!event) {
				throw new Error404();
			}

			res.render('hrRequests/create.nunj', {
				data: req.body,
				event: event,
				departments: DepartmentEnum
			});
		}).catch(next);
	}

	const request = new HrRequest({
		department: req.body.department,
		nbrOfEmployeesNeeded: req.body.nbrOfEmployeesNeeded,
		reason: req.body.reason,
		event: req.params.eventId
	});

	return request.save().then(() => {
		req.flash('success', { msg: 'HR request successfully submitted.' });
		res.redirect('/hr-requests');
	}).catch(next);
};

/**
 * GET /hr-requests
 * Viewing HR requests page
 */
exports.getList = (req, res, next) => {

	const displayArchived = req.query.archived !== undefined && req.query.archived;

	let conditions = {};

	if (!displayArchived) {
		conditions.archived = false;
	}

	if (req.user.role == RolesEnum.PRODUCTION_MANAGER) {
		conditions.department = DepartmentEnum.PRODUCTION;
	} else if (req.user.role == RolesEnum.SERVICE_MANAGER) {
		conditions.department = DepartmentEnum.SERVICES;
	} // IF IT'S NOT PRODUCTION OR SERVICE THEN IT'S HR MANAGER IN WHICH CASE WE DON'T FILTER REQUESTS

	return HrRequest.find(conditions).populate('event').exec().then(requests => {
		res.render('hrRequests/list.nunj', {
			requests: requests,
			displayArchived: displayArchived
		});
	}).catch(next);
};

/**
 * GET /hr-requests/:requestId
 * Viewing an HR request's details page
 */
exports.getDetails = (req, res, next) => {
	req.checkParams('requestId', 'The given HR request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}

	return HrRequest.findOne({ _id: req.params.requestId }).populate('event').exec().then(request => {
		if(!request) {
			throw new Error404();
		}

		res.render('hrRequests/details.nunj', { request: request });
	}).catch(next);
};

/**
 * GET /hr-requests/:requestId/accept
 * Accept an HR request as the senior HR manager
 */
exports.getAccept = (req, res, next) => {
	req.checkParams('requestId', 'The given HR request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}

	return HrRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This HR Request has already been archived and cannot be accepted!'});
			throw new RedirectError('/hr-requests');
		}

		return HrRequest.update({ _id: req.params.requestId }, {
			$set: { archived: true }
		});
	}).then(() => {
		req.flash('success', { msg: 'HR request successfully accepted.' });
		return res.redirect('/hr-requests');
	}).catch(next);
};

/**
 * GET /hr-requests/:requestId/reject
 * Reject an HR request as he senior HR manager
 */
exports.getReject = (req, res, next) => {
	req.checkParams('requestId', 'The given HR request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}

	return HrRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This HR request has already been archived and cannot be rejected!'});
			throw new RedirectError('/hr-requests');
		}

		return HrRequest.update({ _id: req.params.requestId }, {
			$set: {
				rejected: true,
				archived: true
			}
		});
	}).then(() => {
		req.flash('success', { msg: 'HR request successfully rejected.' });
		return res.redirect('/hr-requests');
	}).catch(next);
};

/**
 * GET /hr-requests/:requestId/update
 * Updating an HR request page
 */
exports.getUpdate = (req, res, next) => {
	req.checkParams('requestId', 'The given HR request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}

	return HrRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		} else if (request.archived) {
			req.flash('error', {msg: 'This HR request has already been archived and cannot be updated!'});
			throw new RedirectError('/hr-requests');
		}

		res.render('hrRequests/create.nunj', {
			data: request,
			departments: DepartmentEnum
		});
	}).catch(next);
};

/**
 * POST /hr-requests/:requestId/update
 * Submitting the updated HR request data
 */
exports.postUpdate = (req, res, next) => {
	req.checkBody('department', 'The department must be specified').notEmpty();
	req.checkBody('nbrOfEmployeesNeeded', 'The number of employees must be a positive integer').isPositiveInt();
	req.checkBody('reason', 'The reason for the request cannot be empty').notEmpty();

	req.checkParams('requestId', 'The given HR request id is not a valid id').isMongoId();
	req.sanitizeParams('requestId').toMongoId();
	const errors = req.validationErrors();
	if (errors) {
		req.flash('errors', errors);
		return res.redirect('/hr-requests');
	}
;
	return HrRequest.findOne({ _id: req.params.requestId }).exec().then(request => {
		if (!request) {
			throw new Error404();
		}

		if (request.archived) {
			req.flash('error', {msg: 'This HR request has already been archived and cannot be updated!'});
			throw new RedirectError('/hr-requests');
		}

		return HrRequest.update({ _id: req.params.requestId }, {
			$set: {
				department: req.body.department,
				nbrOfEmployeesNeeded: req.body.nbrOfEmployeesNeeded,
				reason: req.body.reason
			}
		});
	}).then(() => {
		req.flash('success', { msg: 'HR request successfully updated.' });
		res.redirect('/hr-requests');
	}).catch(next);
};
