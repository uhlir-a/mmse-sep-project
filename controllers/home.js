/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  res.render('home.nunj', {
    title: 'Home'
  });
};
