const Event = require('../models/Event');
const Client = require('../models/Client');
const User = require('../models/User');
const Task = require('../models/Task');
const PreferencesEnum = require('../enums/PreferencesEnum');
const PrioritiesEnum = require('../enums/PrioritiesEnum');
const RedirectError = require('../tools/RedirectError');
const Error404 = require('../tools/Error404');

const validateForm = req => {
    "use strict";

    req.checkBody('eventType', 'Event type cannot be empty!').notEmpty();
    req.checkBody('description', 'Event description cannot be empty!').notEmpty();
    req.checkBody('dateFrom', '`Date from` cannot be empty!').notEmpty();
    req.checkBody('dateFrom', '`Date from` "%0" is not a date! It has to have format YYYY-MM-DD.').isDate();
    req.checkBody('dateFrom', '`Date from` "%0" is not a date in future!').isDate().isFutureDate();
    req.checkBody('dateTo', '`Date to` cannot be empty!').notEmpty();
    req.checkBody('dateTo', '`Date to` "%0" is not a date! It has to have format YYYY-MM-DD.').isDate();
    req.checkBody('dateTo', '`Date to` "%0" is not a date in future!').isDate().isFutureDate();
    req.checkBody('dateFrom', '`Date to` has to be further then `Data from`').isDateFurtherOrEqual(req.body.dateTo);
    req.checkBody('numberOfAttendees', 'Number of attendees cannot be empty and has to be bigger than 0!').notEmpty().isPositiveInt();
    req.checkBody('budget', 'Budget "%0" has to be not empty and has to be a positive number!').notEmpty().isPositiveNumeric();
    req.checkBody('tasks', 'Tasks has to be an object!').isObject();
    req.checkBody('client', 'Client has to be an ObjectId!').isMongoId();
    req.sanitizeBody('client').toMongoId();

    return req.validationErrors();
};
exports.validateForm = validateForm;

const isIdValid = (req, res, redirectUrl = '/events') => {
    req.checkParams('eventId', 'Event Id has to be set and valid Id!').isMongoId();
    req.sanitizeParams('eventId').toMongoId();
    let errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect(redirectUrl); // TODO: Redirect back from where he came (not everybody will have access to List of Events
        return false;
    }

    return true;
};
exports.isIdValid = isIdValid;


exports.getList = (req, res, errorHandler) => {
    "use strict";

    const displayArchived = req.query.archived !== undefined && req.query.archived;

    return Event.find({archived: displayArchived}).populate('client').exec()
        .then(events => {
            res.render('events/list.nunj', {
                events: events,
                displayArchieved: displayArchived
            });
        }).catch(errorHandler);
};

exports.getDetail = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    let eventPromise = Event.findOne({_id: req.params.eventId}).populate('client tasks approvedBy').exec();
    let usersPromise = User.find().exec(); // TODO: Filtr just Users assigned to some team?

    return Promise.all([eventPromise, usersPromise])
        .then(([event, users] )=> {
            if (!event) {
                throw new Error404();
            }

            res.render('events/detail.nunj', {
                event: event,
                users: users,
                priorities: PrioritiesEnum._toArray()
            })
        }).catch(errorHandler);
};

exports.getUpdate = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    const event = Event.findOne({_id: req.params.eventId}).populate('tasks').exec();
    const clients = Client.find().exec();

    return Promise.all([event, clients])
        .then(([event, clients]) => {
            if (!event) {
                throw new Error404();
            }

            res.render('events/update.nunj', {
                data: event,
                tasks: PreferencesEnum,
                clients: clients
            });
        }).catch(errorHandler)
};

let rebuildTasks = (tasks, toObject = false) => {
    let newTasks = (toObject ? {} : []);
    for (var taskName in tasks) {
        if (tasks.hasOwnProperty(taskName)) {
            if (toObject){
                newTasks[taskName] = tasks[taskName];
            }else{
                newTasks.push({name: taskName, description: tasks[taskName]});
            }
        }
    }
    return newTasks;
};

exports.postUpdate = (req, res, errorHandler) => {
    "use strict";

    req.checkParams('eventId', 'Event Id has to be set and valid Id!').isMongoId();
    const errors = validateForm(req);

    let data = req.body;

    if (errors) {
        req.flash('errors', errors);
        Client.find().exec()
            .then(clients => {
                data.tasks = rebuildTasks(data.tasks);
                res.render('events/update.nunj', {
                    clients: clients,
                    tasks: PreferencesEnum,
                    data: data
                });
            }).catch(errorHandler);
        return;
    }

    return Event.findOne({_id: req.params.eventId}).populate('tasks').exec()
        .then(event => {
            if(event.archived){
                req.flash(req.flash('error', {msg: 'You can not assign tasks, when the Event is already archived!'}));
                throw new RedirectError(req.url);
            }

            let tasksPromises = [], editedTasks = rebuildTasks(data.tasks, true);
            for(let task of event.tasks){
                task.description = editedTasks[task.name];
                tasksPromises.push(task.save());
            }

            // Copy new data into Event Mongoose object
            delete data.tasks; // Tasks are handled before
            Object.assign(event, data);

            return Promise.all([event.save(), ...tasksPromises]);
        })
        .then(() => {
            req.flash('success', {msg: 'Event successfully updated!'});
            res.redirect('/events/' + req.params.eventId);
        })
        .catch(errorHandler);
};

exports.getArchive = (req, res, errorHandler) => {
    "use strict";

    if(!isIdValid(req,res)){
        return;
    }

    // TODO: Check if user has proper role for this
    return Event.update({_id: req.params.eventId}, {archived: true})
        .then(() => {
            req.flash('success', {msg: 'Event successfully archived!'});
            res.redirect('/events');
        })
        .catch(errorHandler);
};

exports.postAssignTask = (req, res, errorHandler) => {
    "use strict";

    req.checkParams('eventId', 'Event Id has to be set and valid Id!').isMongoId();
    req.checkBody('taskId', 'Task Id has to be set and valid Id!').isMongoId();
    req.checkBody('priority', 'Task Priority has to be set!').notEmpty();
    req.checkBody('assignedTo', 'User Id of assigned user has to be set and valid Id!').isMongoId();
    req.checkBody('budget', 'Budget has to be set and has to be positive!').notEmpty().isPositiveNumeric();
    req.sanitizeBody('assignedTo').toMongoId();
    req.sanitizeBody('taskId').toMongoId();
    req.sanitizeParams('eventId').toMongoId();

    let errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        return res.redirect(req.url);
    }

    let data = Object.assign({}, req.body);
    data.assignedBy = req.user._id;
    delete data.taskId;

    return Event.findOne({_id: req.params.eventId}).exec()
        .then(event => {
            if(event.archived){
                req.flash(req.flash('error', {msg: 'You can not assign tasks, when the Event is already archived!'}));
                throw new RedirectError(req.url);
            } else if (req.body.budget > event.budget) {
				req.flash(req.flash('error', {msg: 'The task budget cannot be larger than the event budget.'}));
                throw new RedirectError(req.url);
			}

            return Task.update({_id : req.body.taskId}, data).exec()
        })
        .then(() => {
            req.flash('success', {msg: 'Task successfully assigned!'});
            res.redirect(req.url);
        }).catch(errorHandler);
};
