const async = require('async');
const crypto = require('crypto');
const passport = require('passport');
const User = require('../models/User');
const RolesEnum = require('../enums/RolesEnum');
const RedirectError = require('../tools/RedirectError');

/**
 * GET /login
 * Login page.
 */
exports.getLogin = (req, res) => {
  if (req.user) {
    return res.redirect('/');
  }
  res.render('users/login.nunj', {
    title: 'Login'
  });
};

/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = (req, res, next) => {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/login');
  }

  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      req.flash('errors', info);
      return res.redirect('/login');
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
      req.flash('success', { msg: 'Success! You are logged in.' });
      res.redirect(req.session.returnTo || '/');
    });
  })(req, res, next);
};

/**
 * GET /logout
 * Log out.
 */
exports.logout = (req, res) => {
  req.logout();
  res.redirect('/');
};

/**
 * GET /users
 * Users list page.
 */
exports.getList = (req, res, errorHandler) => {
  "use strict";

  return User.find().exec()
      .then(users => {
        res.render('users/list.nunj', {
          'users': users
        })
      }).catch(errorHandler);
};

/**
 * GET /users/create
 * Signup page.
 */
exports.getCreate = (req, res) => {
  res.render('users/signup.nunj', {
    title: 'Create Account',
    roles: RolesEnum._toArray()
  });
};

/**
 * POST /users/create
 * Create a new local account.
 */
exports.postCreate = (req, res, next) => {
  req.assert('email', 'Email is not valid!').isEmail();
  req.assert('name', 'Email is not valid!').notEmpty();
  req.assert('role', 'Role has to be set!').notEmpty();
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.render('users/signup.nunj', {
      title: 'Create Account',
      data: req.body,
      roles: RolesEnum._toArray()
    });
  }

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role
  });

  return User.findOne({ email: req.body.email }).exec()
      .then(existingUser => {
          if (existingUser) {
              req.flash('errors', {msg: 'Account with that email address already exists.'});
              throw new RedirectError('/users/create');
          }

          return user.save();
      }).then(()=> {
          req.flash('success', {msg: 'User succesfully created!'});
          res.redirect('/users/create');
      }).catch(next);
};
