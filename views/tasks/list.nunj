{% extends 'base.nunj' %}

{% set page = "Tasks" %}

{% block title %}{{ page }}{% endblock %}

{% block content %}
    <div class="row mb-10">
        <div class="col-md-3">
            {% if displayCompleted %}
                <a href="/tasks" class="btn btn-link">Display active tasks</a>
            {% else %}
                <a href="/tasks/?completed=true" class="btn btn-link">Display completed tasks</a>
            {% endif %}
        </div>
        <div class="col-md-6">
            <h1 class="text-center no-margin">{{ page }}</h1>
        </div>
    </div>

    {% if tasks | length %}
        <table class="table table-striped">
            <tr>
                <th>Task Subject</th>
                <th>Priority</th>
                <th>Sender</th>
                <th>Budget</th>
                <th></th>
            </tr>
            {% for task in tasks %}
                <tr>
                    <td>{{ task.name | capitalize }} for <a href="/events/{{ task.event._id }}">{{ task.event.eventType }}</a></td>
                    <td>{{ task.priority | capitalize }}</td>
                    <td>{{ task.assignedBy.name }}</td>
                    <td>{{ task.budget}}</td>
                    <td class="text-right">
                        <a href="#" class="btn btn-default" data-description="<div style='white-space: pre;'>{{ task.description }}</div>" data-title="{{ task.name | capitalize }} for {{ task.event.eventType }}">Detail</a>
                        {% if not task.completed %}
                            <a href="/tasks/{{ task._id }}/complete" class="btn btn-success">Completed</a>
                            <a href="/resource-requests/{{ task._id }}/create/" class="btn btn-danger">Resource request</a>
                        {% endif %}
                    </td>
                </tr>
            {% endfor %}
        </table>
    {% else %}
        <p class="text-center">No tasks to display.</p>
    {% endif %}
{% endblock %}

{% block javascript %}
    <script type="text/javascript">
        $(document).ready(function(){
            $('a[data-description]').click(function(e){
                e.preventDefault();

                bootbox.alert({
                    title: $(this).data('title'),
                    message: $(this).data('description')
                })
            })
        });
    </script>
{% endblock %}