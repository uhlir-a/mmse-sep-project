{% extends "base.nunj" %}

{% set page = "HR Requests" %}

{% block title %}{{ page }}{% endblock %}

{% block content %}
	<div class="row mb-10">
		<div class="col-md-3">
            {% if displayArchived %}
                <a href="/hr-requests" class="btn btn-link">Show only pending HR requests</a>
            {% else %}
                <a href="/hr-requests?archived=true" class="btn btn-link">Show archived HR requests</a>
            {% endif %}
        </div>
		<div class="col-md-6">
			<h1 class="text-center no-margin">{{ page }}</h1>
		</div>
		<div class="col-md-3">
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{% if requests.length %}
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Event date range</th>
						<th>Department</th>
						<th>Requested employees</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{% for request in requests %}
					<tr>
						<td><a href="/events/{{ request.event._id }}">{{ request.event.dateFrom | date }} - {{ request.event.dateTo | date }}</a></td>
						<td>{{ request.department | capitalize }}</td>
						<td>{{ request.nbrOfEmployeesNeeded }}</td>
						<td>
							{% if request.archived %}
							{% if request.rejected %}
							<p style="color:red;">Rejected</p>
							{% else %}
							<p style="color:green;">Accepted</p>
							{% endif %}
							{% else %}
							<p style="color:grey;">Pending</p>
							{% endif %}
						</td>
						<td class="text-right">
							{% if user | hasRole([roles.DEPARTMENT_MANAGER]) and not request.archived %}
							<a href="/hr-requests/{{ request._id.toString() }}/update" class="btn btn-info">Update</a>
							{% endif %}
							<a href="/hr-requests/{{ request._id.toString() }}" class="btn btn-default">View Details</a>
						</td>
					</tr>
					{% endfor %}
				</tbody>
			</table>
			{% else %}
			<p class="text-center">No {{ "pending " if not displayArchived }}HR requests.</p>
			{% endif %}
		</div>
	</div>
{% endblock %}
