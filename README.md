# SEP Project

Implementation of school project of imaginary business SEP.

## Build&Run

### Docker

The easy way is using Docker:

 1. Copy `.env.example` file, rename it to `.env` and set variables in it.
 2. Install `docker` and `docker-compose`.
 3. Build containers with `docker-compose build`. When running first time, it might take some time, to download required files.
 4. Run containers with `docker-compose up`.
 5. Fill the DB with default admin user with command `docker-compose run server nodejs fillDb.js`
 6. Access the webpage on `localhost:8000`, where the admin user has login credentials `admin@admin.com` and password `admin`. 
### Set up your own enviroment

But if you have problem with Docker (for example because of your OS), then you can set up environment in following way:

 1. Install Node.js (version 6.3) and NPM
 2. Install MongoDB
 3. Copy `.env.example` file, rename it to `.env` and set variables in it (mostly DB access).
 4. Run `npm install` and then `bower install` (if it doesn't work install bower globally - `npm install -g bower`)
 5. Build static files `gulp build`.
 6. Run the server with `npm start`.
 7. Fill the DB with default admin user with command `nodejs fillDb.js`. 
 8. Access the webpage on `localhost:8000`, where the admin user has login credentials `admin@admin.com` and password `admin`.
 
## Tests

To run unit&integration tests use `npm test` command, or if running through Docker with `docker-compose run server npm test`.

### Acceptance tests

To run acceptance tests, you have to install Selenium IDE (Firefox plugin) and then load the `test_suite` file in `/test/acceptance` folder.

## Tools&Resources

 * [Folder structure](https://www.terlici.com/2014/08/25/best-practices-express-structure.html)
 * [Server boilerplate](https://github.com/sahat/hackathon-starter)
 * [Templating engine](https://mozilla.github.io/nunjucks)
 * [ES6](https://medium.com/sons-of-javascript/javascript-an-introduction-to-es6-1819d0d89a0f#.5xoptj4p9)
 * [Promises](https://www.sitepoint.com/overview-javascript-promises/)
 * [Mongoose - layer between DB (MongoDB) and Javascript](http://mongoosejs.com/docs/index.html)
 * [Mocha + Chai - test framework](https://www.sitepoint.com/unit-test-javascript-mocha-chai/)
 * [Bootstrap](http://getbootstrap.com/)
 
