$(document).ready(function () {
    $('input[data-type=\'date\']').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: Date.now()
    });

    $('a[data-confirm]').click(function (e) {
        e.preventDefault();
        var title = $(this).data('title');

        bootbox.confirm({
            message: title,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if(result) {
                    window.location.href = $(this).attr('href');
                }
            }.bind(this)
        });
    });
});