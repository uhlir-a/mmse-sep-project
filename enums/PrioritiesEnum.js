
module.exports = {
    HIGH: 'high',
    MIDDLE: 'middle',
    LOW: 'low',
    _toArray: function (values = true) {
        return Object.keys(this).filter(value => !value.startsWith('_')).map(key => (values ? this[key] : key));
    }
};
