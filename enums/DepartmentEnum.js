
module.exports = {
    PRODUCTION: 'production',
    SERVICES: 'services',
    _toArray: function (values = true) {
        return Object.keys(this).filter(value => !value.startsWith('_')).map(key => (values ? this[key] : key));
    }
};
