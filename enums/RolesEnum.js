module.exports = {
    TEAM_MEMBER: 'teamMember',
    DEPARTMENT_MANAGER: 'departmentManager',
    PRODUCTION_MANAGER: 'productionManager',
    SERVICE_MANAGER: 'serviceManager',
    CUSTOMER_SERVICE: 'customerService',
    SENIOR_CUSTOMER_SERVICE: 'seniorCustomerService',
    FINANCIAL_MANAGER: 'financialManager',
    ADMINISTRATION_TEAM: 'administrationTeam',
    ADMINISTRATION_MANAGER: 'administrationManager',
    HR_TEAM: 'hrTeam',
    ADMIN: 'admin',

    _inheritance: [
        {
            name: 'teamMember', // Tasks
            inherit: ''
        },
        {
            name: 'departmentManager', // Event Update, Assigning Tasks, Review/Reject ResourceRequest
            inherit: ''
        },
        {
            name: 'productionManager',
            inherit: 'departmentManager'
        },
        {
            name: 'serviceManager',
            inherit: 'departmentManager'
        },
        {
            name: 'customerService', // Client Management, Create Event Request
            inherit: ''
        },
        {
            name: 'seniorCustomerService', // customer service + Event Update, Review/Reject Event Requests
            inherit: 'customerService'
        },
        {
            name: 'financialManager', // customer service + Resource Request Accept/Reject
            inherit: 'customerService'
        },
        {
            name: 'administrationTeam', // customer service + Archive Events
            inherit: 'customerService'
        },
        {
            name: 'administrationManager', // administration team + Accept/Reject Event Requests
            inherit: 'administrationTeam'
        },
        {
            name: 'hrTeam', // HR Requests
            inherit: ''
        },
        {
            name: 'admin',
            inherit: ['administrationManager', 'financialManager', 'departmentManager', 'teamMember', 'hrTeam']
        }
    ],
    _toArray: function (values = true) {
        return Object.keys(this).filter(value => !value.startsWith('_')).map(key => (values ? this[key] : key));
    }
};