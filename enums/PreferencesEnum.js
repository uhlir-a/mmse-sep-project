
module.exports = {
    DECORATION: 'decoration',
    PARTIES: 'parties',
    DOCUMENTATION: 'documentation',
    BEVERAGES: 'beverages',
    FOOD: 'food',
    _toArray: function (values = true) {
        return Object.keys(this).filter(value => !value.startsWith('_')).map(key => (values ? this[key] : key));
    }
};