const mongoose = require('mongoose');
const DepartmentEnum = require('../enums/DepartmentEnum');

const resourceRequestSchema = new mongoose.Schema({
	requester: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	department: {
		type: String,
		enum: DepartmentEnum._toArray()
	},
	task: {type: mongoose.Schema.Types.ObjectId, ref: 'Task'},
	newBudget: Number,
	reason: String,
	archived: {
		type: Boolean,
		default: false
	},
	approvedByDptManager: {
		type: Boolean,
		default: false
	},
	rejected: {
		type: Boolean,
		default: false
	}
});

const ResourceRequest = mongoose.model('ResourceRequest', resourceRequestSchema);

module.exports = ResourceRequest;
