const mongoose = require('mongoose');
const PrioritiesEnum = require('../enums/PrioritiesEnum');

const eventSchema = new mongoose.Schema({
	eventType: String,
	dateFrom: Date,
	dateTo: Date,
	numberOfAttendees: Number,
	tasks: [{type: mongoose.Schema.Types.ObjectId, ref: 'Task'}],
	budget: Number,
	client: {type: mongoose.Schema.Types.ObjectId, ref: 'Client'},
	description: String,
	archived: {
		type: Boolean,
		default: false
	},
	approvedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

const Event = mongoose.model('Event', eventSchema);
module.exports = Event;
