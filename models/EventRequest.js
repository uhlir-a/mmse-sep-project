const mongoose = require('mongoose');

const eventRequestSchema = new mongoose.Schema({
	eventType: String,
	dateFrom: Date,
	dateTo: Date,
	numberOfAttendees: Number,
	preferences: [String],
	budget: Number,
	client: {type: mongoose.Schema.Types.ObjectId, ref: 'Client'},
    reviews: [{
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        feedback: String,
        createdAt: {
            type: Date,
            default: Date.now
        }
    }],
	rejected: {
		type: Boolean,
		default: false
	},
	createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

eventRequestSchema.methods.userHaveReviewed = function(user){
	return this.reviews.filter(review => {return review.user.equals(user._id)}).length > 0;
};

const EventRequest = mongoose.model('EventRequest', eventRequestSchema);
module.exports = EventRequest;
