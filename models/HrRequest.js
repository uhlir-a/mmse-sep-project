const mongoose = require('mongoose');
const DepartmentEnum = require('../enums/DepartmentEnum');

const hrRequestSchema = new mongoose.Schema({
	department: {
		type: String,
		enum: DepartmentEnum._toArray()
	},
	nbrOfEmployeesNeeded: Number,
	reason: String,
	event: {type: mongoose.Schema.Types.ObjectId, ref: 'Event'},
	archived: {
		type: Boolean,
		default: false
	},
	rejected: {
		type: Boolean,
		default: false
	}
});

const HrRequest = mongoose.model('HrRequest', hrRequestSchema);

module.exports = HrRequest;
