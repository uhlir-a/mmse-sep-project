const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
	name: String,
	email: { type: String, unique: true },
	phoneNumber: String
});

const Client = mongoose.model('Client', clientSchema);

module.exports = Client;
