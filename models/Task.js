const mongoose = require('mongoose');
const PrioritiesEnum = require('../enums/PrioritiesEnum');

const taskSchema = new mongoose.Schema({
    name: String,
    event: {type: mongoose.Schema.Types.ObjectId, ref: 'Event'},
    description: String,
    budget: {
        type: Number,
        default: 0
    },
    assignedTo: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    assignedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    priority: {
        type: String,
        enum: PrioritiesEnum._toArray()
    },
    completed: {
        type: Boolean,
        default: false
    }
});

const Task = mongoose.model('Task', taskSchema);
module.exports = Task;
