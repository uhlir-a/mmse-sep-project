const dotenv = require('dotenv');
const mongoose = require('mongoose');
const chalk = require('chalk');

const User = require('./models/User');

dotenv.load({path: '.env'});
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
mongoose.connection.on('connected', () => {
    console.log('%s MongoDB connection established!', chalk.green('✓'));


    const admin = new User({
        name: 'Admin',
        email: 'admin@admin.com',
        password: 'admin',
        role: 'admin'
    });

    admin.save()
        .then(() => {
            console.log('%s Admin User created!', chalk.green('✓'));
            process.exit();
        })
        .catch(() => {
            console.log('%s Error while user creation!', chalk.red('✗'));
            process.exit();
        });

});
mongoose.connection.on('error', () => {
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});