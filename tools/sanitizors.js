const mongoose = require('mongoose');

module.exports = {
    toMongoId : id => {
        if(!id) return undefined;
        try {
            return mongoose.Types.ObjectId(id);
        }catch(e){
            return undefined;
        }
    }
};