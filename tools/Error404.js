class ExtendableError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        this.message = message;
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }
}

class Error404 extends ExtendableError {
    constructor() {
        super('Not found!');
        this.statusCode = 404;
    }
}

module.exports = Error404;