const validators = require('validator');

module.exports = {
    isFutureDate: value => {
        "use strict";
        if(value === undefined) return false;
        let date = new Date(value);
        return date > Date.now()
    },

    isObject: value => {
        "use strict";
        return typeof value == 'object';
    },

    isArray: value => {
        "use strict";
        if(value === undefined) return false;
        return Array.isArray(value);
    },
    
    isPositiveInt: value => {
        "use strict";
        return value !== undefined && validators.isInt(value) && value > 0;
    },
    
    isPositiveNumeric: value => {
        "use strict";
        return value !== undefined && validators.isFloat(value) && value > 0;
    },
    
    isDateFurtherOrEqual: (value, furtherDate) => {
        "use strict";
        return value !== undefined && furtherDate !== undefined && validators.isDate(value) && validators.isDate(furtherDate)
                && value <= furtherDate;
    }
};