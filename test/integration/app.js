const request = require('supertest-as-promised')(Promise);
const app = require('../../app.js');
const roles = require('../../enums/RolesEnum');
const User = require('../../models/User');

describe('[Authorization]', () => {
    "use strict";

    const initTemplate = function(role, agent){
        return () => {
            return agent
                .post('/users/create')
                .type('form')
                .send({
                    email: role + '@dev.cz',
                    role: role,
                    name: role,
                    password: '1234',
                    confirmPassword: '1234'
                })
                .then(() => {
                    return agent
                        .post('/login')
                        .type('form')
                        .send({
                            email: role + '@dev.cz',
                            password: '1234'
                        })
                        .expect(302)
                })
        }
    };

    const acceptTemplate = function(agent)  {
        return function () {
            const parsedTitle = this.test.title.split(' ');

            if (parsedTitle[0].toLowerCase() == 'post') {
                return agent
                    .post(parsedTitle[1])
                    .type('form')
                    .send({})
                    .expect(200);
            } else {
                return agent
                    .get(parsedTitle[1])
                    .expect(200);
            }
        }
    };

    const notFoundTemplate = function(agent)  {
        return function(){
            const parsedTitle = this.test.title.split(' ');

            if (parsedTitle[0].toLowerCase() == 'post') {
                return agent
                    .post(parsedTitle[1])
                    .type('form')
                    .send({})
                    .expect(404);
            } else {
                return agent
                    .get(parsedTitle[1])
                    .expect(404);
            }
        }
    };

    const rejectTemplate = function(agent){
        return function() {
            const parsedTitle = this.test.title.split(' ');

            if (parsedTitle[0].toLowerCase() == 'post') {
                return agent
                    .post(parsedTitle[1])
                    .expect(302)
                    .expect('Location', ('/'));
            } else {
                return agent
                    .get(parsedTitle[1])
                    .expect(302)
                    .expect('Location', ('/'));
            }
        };
    };

    after('Remove all created users', () => {
        return User.remove();
    });

    describe('(Team member)', () => {
        const role = roles.TEAM_MEMBER;
        const roleName = 'Team member';
        const agent = request.agent(app);
        const accept = acceptTemplate(agent);
        const reject = rejectTemplate(agent);
        const notFound = notFoundTemplate(agent);

        before('Init ' + roleName + ' User', initTemplate(role, agent));

        it('GET / should be 200 OK', accept);

        // Clients
        it('GET /clients/create should be 302 Redirect', reject);
        it('POST /clients/create should be 302 Redirect', reject);
        it('GET /clients should be 302 Redirect', reject);
        it('GET /clients/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('POST /clients/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('GET /clients/57ff59d2d44bec029342fd0e should be 302 Redirect', reject);

        // Event Requests
        it('GET /event-requests/create should be 302 Redirect', reject);
        it('POST /event-requests/create should be 302 Redirect', reject);
        it('GET /event-requests should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('POST /event-requests/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/review should be 302 Redirect', reject);
        it('POST /event-requests/57ff59d2d44bec029342fd0e/review should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/reject should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/accept should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e should be 302 Redirect', reject);

        // Events
        it('GET /events should be 200 OK', accept);
        it('GET /events/57ff59d2d44bec029342fd0e should be 404 Not found', notFound);
        it('GET /events/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('POST /events/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('GET /events/57ff59d2d44bec029342fd0e/archive should be 302 Redirect', reject);

        // Tasks
        it('GET /tasks should be 200 OK', accept);
        it('GET /tasks/57ff59d2d44bec029342fd0e/complete should be 404 Not found', notFound);

        // Resource Requests
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/create should be 404 Not found', notFound);
        it('GET /resource-requests/57ff59d2d44bec029342fd0e should be 404 Not found', notFound);
        it('GET /resource-requests should be 200 OK', accept);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/approve should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/accept should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/reject should be 302 Redirect', reject);
    });


    describe('(Customer service)', () => {
        const role = roles.CUSTOMER_SERVICE;
        const roleName = 'Customer service';
        const agent = request.agent(app);
        const accept = acceptTemplate(agent);
        const reject = rejectTemplate(agent);
        const notFound = notFoundTemplate(agent);

        before('Init ' + roleName + ' User', initTemplate(role, agent));

        it('GET / should be 200 OK', accept);

        // Clients
        it('GET /clients/create should be 200 OK', accept);
        it('POST /clients/create should be 200 OK', accept);
        it('GET /clients should be 200 OK', accept);
        it('GET /clients/57ff59d2d44bec029342fd0e/update should be 404 Not found', notFound);
        it('POST /clients/57ff59d2d44bec029342fd0e/update should be 200 OK', accept);
        it('GET /clients/57ff59d2d44bec029342fd0e should be 404 Not found', notFound);

        // Event Requests
        it('GET /event-requests/create should be 200 OK', accept);
        it('POST /event-requests/create should be 200 OK', accept);
        it('GET /event-requests should be 200 OK', accept);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/update should be 404 Not found', notFound);
        it('POST /event-requests/57ff59d2d44bec029342fd0e/update should be 200 OK', accept);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/review should be 302 Redirect', reject);
        it('POST /event-requests/57ff59d2d44bec029342fd0e/review should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/reject should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e/accept should be 302 Redirect', reject);
        it('GET /event-requests/57ff59d2d44bec029342fd0e should be 404 Not found', notFound);

        // Events
        it('GET /events should be 200 OK', accept);
        it('GET /events/57ff59d2d44bec029342fd0e should be 404 Not found', notFound);
        it('GET /events/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('POST /events/57ff59d2d44bec029342fd0e/update should be 302 Redirect', reject);
        it('GET /events/57ff59d2d44bec029342fd0e/archive should be 302 Redirect', reject);

        // Tasks
        it('GET /tasks should be 302 Redirect', reject);
        it('GET /tasks/57ff59d2d44bec029342fd0e/complete should be 302 Redirect', reject);

        // Resource Requests
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/create should be 302 Redirect', reject);
        it('POST /resource-requests/57ff59d2d44bec229342fd0e/create should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec029342fd0e should be 302 Redirect', reject);
        it('GET /resource-requests should be 302 Redirect', reject);
        it('POST /resource-requests/57ff59d2d44bec229342fd0e/update should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/approve should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/accept should be 302 Redirect', reject);
        it('GET /resource-requests/57ff59d2d44bec229342fd0e/reject should be 302 Redirect', reject);
    });
});


describe('GET /', () => {
    it('should return 302 Redirect to Log in', (done) => {
        request(app)
            .get('/')
            .expect(302)
            .expect('Location', '/login', done);
    });
});

describe('GET /login', () => {
    it('should return 200 OK', (done) => {
        request(app)
            .get('/login')
            .expect(200, done);
    });
});

describe('GET /random-url', () => {
    it('should return 404', (done) => {
        request(app)
            .get('/reset')
            .expect(404, done);
    });
});

