const assert = require('chai').assert;

const RedirectError = require('../../../tools/RedirectError');
describe("RedirectError Class", () => {
    "use strict";

    it("", () => {
        const err = new RedirectError('/');

        assert.equal(err.url, '/');
        assert.equal(err.message, 'Redirecting to /');
        assert.instanceOf(err, RedirectError);
    });
});
