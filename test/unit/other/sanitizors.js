const assert = require('chai').assert;

const sanitizors = require('../../../tools/sanitizors');

describe("Custom Sanitizors", () => {
    "use strict";

    it("toMongoId", () => {
        assert.isUndefined(sanitizors.toMongoId());
        assert.isUndefined(sanitizors.toMongoId(''));
        assert.isUndefined(sanitizors.toMongoId('ASD'));
        assert.isUndefined(sanitizors.toMongoId('580209b20f127907881c716bA'));
        assert.isUndefined(sanitizors.toMongoId('580209b20f127907881c7'));
        assert.isDefined(sanitizors.toMongoId('580209b20f127907881c716b'));
        assert.isObject(sanitizors.toMongoId('580209b20f127907881c716b'));
    });
});
