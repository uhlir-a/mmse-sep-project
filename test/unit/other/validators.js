const assert = require('chai').assert;

const validators = require('../../../tools/validators');
describe("Custom Validators", () => {
    "use strict";

    it("isFutureDate", () => {
        assert.isFalse(validators.isFutureDate('1999-12-12'));
        assert.isTrue(validators.isFutureDate('2099-12-12'));
        assert.isFalse(validators.isFutureDate());
    });

    it("isObject", () => {
        assert.isFalse(validators.isObject('asd'));
        assert.isFalse(validators.isObject());
        assert.isFalse(validators.isObject(123));
        assert.isTrue(validators.isObject({}));
        assert.isTrue(validators.isObject({asd: 'asd'}));
    });

    it("isArray", () => {
        assert.isFalse(validators.isArray('asd'));
        assert.isFalse(validators.isArray(123));
        assert.isFalse(validators.isArray({}));
        assert.isFalse(validators.isArray());
        assert.isFalse(validators.isArray({aasd: 'asd'}));
        assert.isTrue(validators.isArray([]));
        assert.isTrue(validators.isArray(['asd']));
    });

    it("isPositiveInt", () => {
        assert.isFalse(validators.isPositiveInt());
        assert.isFalse(validators.isPositiveInt('-1'));
        assert.isFalse(validators.isPositiveInt('-1.5'));
        assert.isFalse(validators.isPositiveInt('0'));
        assert.isFalse(validators.isPositiveInt('0.5'));
        assert.isFalse(validators.isPositiveInt('1.5'));
        assert.isTrue(validators.isPositiveInt('1'));
    });

    it("isPositiveNumeric", () => {
        assert.isFalse(validators.isPositiveNumeric());
        assert.isFalse(validators.isPositiveNumeric(''));
        assert.isFalse(validators.isPositiveNumeric('-0.5'));
        assert.isFalse(validators.isPositiveNumeric('-1'));
        assert.isFalse(validators.isPositiveNumeric('0'));
        assert.isTrue(validators.isPositiveNumeric('0.2'));
        assert.isTrue(validators.isPositiveNumeric('2.5'));
    });

    it("isDateFurtherOrEqual", () => {
        assert.isFalse(validators.isDateFurtherOrEqual());
        assert.isFalse(validators.isDateFurtherOrEqual('2099-12-12'));
        assert.isFalse(validators.isDateFurtherOrEqual('2099-12-12', '2098-12-12'));
        assert.isTrue(validators.isDateFurtherOrEqual('2099-12-12', '2100-12-12'));
    });
});
