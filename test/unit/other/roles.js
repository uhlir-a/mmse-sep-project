const Roles = require('../../../tools/roles');
var chai = require('chai'),
    spies = require('chai-spies');
const assert = chai.assert;
const expect = chai.expect;
chai.use(spies);


const rolesDefinition = [
    {
        name: 'guest',
        inherit: ''
    },
    {
        name: 'user',
        inherit: 'guest'
    },
    {
        name: 'admin',
        inherit: 'user'
    }
];

const RequestMockup = function (userRole) {
    "use strict";

    if (userRole) {
        this.user = {role: userRole};
    }
};

const RespondMockup = function () {
    this.status = 0;

    this.sendStatus = (status) => {
        this.status = status;
    };
};



describe('Roles support', () => {
    "use strict";

    it('provide no roles', () => {
        assert.throws(() => {new Roles()}, 'Roles were not provided');
    });

    it('hasRole function', () => {
        assert.doesNotThrow(() => {new Roles(rolesDefinition)}, 'throw exception even with roles provided');

        let roles = new Roles(rolesDefinition);

        assert.isFunction(roles.hasRole);
        assert.isFunction(roles.hasRole());

        let hasRoleFunction = roles.hasRole('user');
        let spy = chai.spy();

        let res = new RespondMockup();
        let req = new RequestMockup();
        hasRoleFunction(new RequestMockup(), res, spy);
        assert(req.status != 401, 'Does not throw 401 for no role on user');
        expect(spy).to.not.have.been.called();

        res = new RespondMockup();
        req = new RequestMockup('guest');
        hasRoleFunction(req, res, spy);
        assert(req.status != 401, 'Does not throw 401 for when user does not have the role');
        expect(spy).to.not.have.been.called();

        res = new RespondMockup();
        req = new RequestMockup('user');
        hasRoleFunction(req, res, spy);
        assert.isUndefined(req.status, 'Request has set status, even with right role.');
        expect(spy).to.have.been.called();
    });

    it('no existing role', () => {
        let roles = new Roles(rolesDefinition);

        let hasRoleFunction = roles.hasRole('user');
        let spy = chai.spy();

        let res = new RespondMockup();
        let req = new RequestMockup('not-existing-role');
        hasRoleFunction(req, res, spy);
        assert(req.status != 401, 'Does not throw 401 for no role on user');
        expect(spy).to.not.have.been.called();

    });

    it('roles inheritance', () => {
        let roles = new Roles(rolesDefinition);

        let hasRoleFunction = roles.hasRole('user');
        let spy = chai.spy();

        let res = new RespondMockup();
        let req = new RequestMockup('guest');
        hasRoleFunction(req, res, spy);
        assert(req.status != 401, 'Does not throw 401 for no role on user');
        expect(spy).to.not.have.been.called();


        req = new RequestMockup('admin');
        res = new RespondMockup();
        hasRoleFunction(req, res, spy);
        assert.isUndefined(req.status, 'Request has set status, even with when have higher role.');
        expect(spy).to.have.been.called();
        
    });

    it('middleware', () => {
        let req = {};

        const roles = new Roles(rolesDefinition);
        const middlewareFunction = roles.middleware();
        middlewareFunction(req, {}, () => {});

        assert.isFunction(req.hasRole);
    });
    
    it('checkUserRole', () => {
        let roles = new Roles(rolesDefinition);

        assert.isNotOk(roles.checkUserRole('guest', ''));
        assert.isOk(roles.checkUserRole('user', 'guest'));
    })
});