const assert = require('chai').assert;

const Error404 = require('../../../tools/Error404');
describe("Error404 Class", () => {
    "use strict";

    it("", () => {
        const err = new Error404();

        assert.equal(err.statusCode, 404);
        assert.instanceOf(err, Error404);
    });
});
