const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const Client = require('../../../models/Client');
const clientController = require('../../../controllers/client');


describe('Client Controller', () => {
    "use strict";

    let client1, client2;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create clients', () => {
        client1 = new Client({
            email: 'test@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        });

        client2 = new Client({
            email: 'test2@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        });

        return Promise.all([client1.save(), client2.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('getCreate', () => {
        clientController.getCreate(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isFalse(resRedirectSpy.called);
        assert.isFalse(reqFlashSpy.called);
        assert.isTrue(resRenderSpy.called);
    });

    it('getList', () => {
        return clientController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.clients);
                assert.lengthOf(dataSink.renderData.clients, 2);
            });
    });

    it('getUpdate without id', () => {
        clientController.getUpdate(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('getEventHistory with valid data', () => {
        req.params.clientId = client1._id;
        return clientController.getEventHistory(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.client);
                assert.isDefined(dataSink.renderData.events);
            });
    });

    it('getEventHistory without id', () => {
        clientController.getEventHistory(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('validateForm with valid data', () => {
        req.body = {
            name: 'Adam',
            email: 'adam@kth.se',
            phoneNumber: '12345678'
        };

        const errors = clientController.validateForm(req);
        assert.isFalse(errors);
    });

    it('validateForm with invalid email', () => {
        req.body = {
            name: 'Adam',
            email: 'adamakth.se',
            phoneNumber: '12345678'
        };

        const errors = clientController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });

    it('validateForm with invalid email and no phoneNumber', () => {
        req.body = {
            name: 'Adam',
            email: 'adamakth.se'
        };

        const errors = clientController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 2);
    });

    it('validateForm with no data', () => {
        const errors = clientController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 3);
    });

    it('postCreate with valid data', () => {
        req.body = {
            name: 'Adam',
            email: 'adam@kth.se',
            phoneNumber: '12345678'
        };

        return clientController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Client.findOne({email: 'adam@kth.se'}).exec()
                    .then(client => {
                        assert.isNotNull(client);
                        assert.equal(client.name, 'Adam');
                    });
            });
    });

    it('postCreate with duplicated email', () => {
        req.body = {
            name: 'Adam',
            email: 'adam@kth.se',
            phoneNumber: '12345678'
        };

        return clientController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('errors'));
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('postUpdate with valid data', () => {
        req.params.clientId = client1._id;
        req.body = {
            email: 'test-new-email@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        };

        return clientController.postUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

            });
    });

    after('Remove clients', () => {
        return Client.remove({});
    });

});
