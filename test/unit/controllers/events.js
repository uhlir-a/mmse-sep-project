const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const Client = require('../../../models/Client');
const Task = require('../../../models/Task');
const Event = require('../../../models/Event');
const User = require('../../../models/User');

const PreferencesEnum = require('../../../enums/PreferencesEnum');
const PrioritiesEnum = require('../../../enums/PrioritiesEnum');

const eventController = require('../../../controllers/events');


describe('[Events Controller]', () => {
    "use strict";

    let client1, event1, task1, task2, user1;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create data', () => {
        client1 = new Client({
            email: 'test@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        });

        task1 = new Task({
            name: PreferencesEnum.DECORATION
        });

        task2 = new Task({
            name: PreferencesEnum.FOOD
        });

        event1 = new Event({
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: new Date(),
            dateTo: new Date(),
            numberOfAttendees: 1234,
            tasks: [task1._id, task2._id],
            budget: 1234,
            client: client1._id
        });

        task1.event = event1._id;
        task2.event = event1._id;

        user1 = new User({
            email: 'test@gmail.com',
            password: 'password',
            role: 'admin'
        });

        return Promise.all([client1.save(), event1.save(), task1.save(), task2.save(), user1.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('validateForm with valid data', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            tasks: {
                'food': 'Description of task!'
            },
            budget: '1234',
            client: client1._id
        };

        const errors = eventController.validateForm(req);
        assert.isFalse(errors);
    });

    it('validateForm with invalid dates', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '01-07-2018',
            dateTo: '2018-07-AA',
            numberOfAttendees: '1234',
            tasks: {
                'food': 'Description of task!'
            },
            budget: '1234',
            client: client1._id
        };

        const errors = eventController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 4);
    });

    it('validateForm with invalid int values', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: 'ASD',
            tasks: {
                'food': 'Description of task!'
            },
            budget: 'ASD',
            client: client1._id
        };

        const errors = eventController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 2);
    });

    it('validateForm with invalid MongoId', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            tasks: {
                'food': 'Description of task!'
            },
            budget: '1234',
            client: 'ASD'
        };

        const errors = eventController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });

    it('validateForm with dateFrom further then dateTo', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '2018-07-05',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            tasks: {
                'food': 'Description of task!'
            },
            budget: '1234',
            client: client1._id
        };

        const errors = eventController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });


    it('validateForm with no tasks object', () => {
        req.body = {
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            tasks: 'asd',
            budget: '1234',
            client: client1._id
        };

        const errors = eventController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });

    it('getList', () => {
        return eventController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.events);
                assert.lengthOf(dataSink.renderData.events, 1);
            });
    });

    it('isIdValid with valid id', () => {
        req.params.eventId = event1._id;
        const result = eventController.isIdValid(req, res);

        assert.isTrue(result);
        assert.isFalse(errHandler.called);
        assert.isFalse(resRedirectSpy.called);
        assert.isFalse(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('isIdValid with invalid id', () => {
        req.params.eventId = 'asd';
        const result = eventController.isIdValid(req, res);

        assert.isFalse(result);
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('isIdValid with no id', () => {
        const result = eventController.isIdValid(req, res);

        assert.isFalse(result);
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('getDetail with valid existing id', () => {
        req.params.eventId = event1._id;
        return eventController.getDetail(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.event);
            });
    });

    it('getDetail with valid non-existing id', () => {
        req.params.eventId = '5800e73f1c4bf10b21650407';
        return eventController.getDetail(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('getUpdate with valid data', () => {
        req.params.eventId = event1._id;
        return eventController.getUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.data);
                assert.isDefined(dataSink.renderData.clients);
            });
    });

    it('postUpdate with valid data', () => {
        req.params.eventId = event1._id;
        req.body = {
            eventType: 'Some random string',
            description: 'Some event description',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            tasks: {},
            budget: '1234',
            client: client1._id
        };
        req.body.tasks[PreferencesEnum.FOOD] = 'Some description for food task';

        return eventController.postUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Event.findOne({_id: event1._id}).populate('tasks').exec()
                    .then(event => {
                        assert.equal(event.eventType, 'Some random string');
                        assert.equal(event.tasks.find(task => task.name == PreferencesEnum.FOOD).description, 'Some description for food task');
                    });
            });
    });


    it('getArchive', () => {
        req.params.eventId = event1._id;
        return eventController.getArchive(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Event.findOne({_id: event1._id}).exec()
                    .then(event => {
                        assert.isTrue(event.archived);
                    });
            });
    });


    it('postAssignTask with valid data to archived event', () => {
        req.params.eventId = event1._id;
        req.body = {
            taskId: task1._id,
            priority: PrioritiesEnum.HIGH,
            assignedTo: user1._id,
            budget: '1234'
        };

        return eventController.postAssignTask(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('error'));
                assert.isFalse(resRenderSpy.called);

                return Event.update({_id: event1._id}, {archived: false});
            });
    });

    it('postAssignTask with invalid data', () => {
        req.params.eventId = event1._id;
        req.body = {
            taskId: 'ASD',
            assignedTo: '123'
        };

        eventController.postAssignTask(req, res, errHandler);
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('postAssignTask with valid data', () => {
        req.params.eventId = event1._id;
        req.body = {
            taskId: task1._id,
            priority: PrioritiesEnum.HIGH,
            assignedTo: user1._id,
            budget: '1234'
        };

        return eventController.postAssignTask(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Task.findOne({_id: task1._id}).exec()
                    .then(task => {
                        assert.isNotNull(task);
                        assert(task.assignedTo.equals(user1._id));
                        assert(task.assignedBy.equals(req.user._id));
                    });
            });
    });

    after('Remove data', () => {
        return Promise.all([Client.remove({}), Task.remove({}), User.remove({}), Event.remove({})]);
    });

});