const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const Event = require('../../../models/Event');
const HrRequest = require('../../../models/HrRequest');

const PreferencesEnum = require('../../../enums/PreferencesEnum');
const RolesEnum = require('../../../enums/RolesEnum');
const DepartmentEnum = require('../../../enums/DepartmentEnum');

const hrRequestsController = require('../../../controllers/hrRequests');


describe('[HR Requests Controller]', () => {
    "use strict";

    let event1, hrRequest1;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create data', () => {

        event1 = new Event({
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: new Date(),
            dateTo: new Date(),
            numberOfAttendees: 1234,
            tasks: [],
            budget: 1234
        });

        hrRequest1 = HrRequest({
            department: DepartmentEnum.SERVICES,
            nbrOfEmployeesNeeded: 123,
            reason: 'string',
            event: event1._id
        });

        return Promise.all([event1.save(), hrRequest1.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('isIdValid with valid id', () => {
        req.params.eventId = event1._id;
        const result = hrRequestsController.isIdValid(req, res);
        assert.isFalse(result);
    });

    it('isIdValid with invalid id', () => {
        req.params.eventId = 'asd';
        const result = hrRequestsController.isIdValid(req, res);

        assert.isArray(result);
        assert.lengthOf(result,1);
    });

    it('isIdValid with no id', () => {
        const result = hrRequestsController.isIdValid(req, res);

        assert.isArray(result);
        assert.lengthOf(result,1);
    });

    it('getCreate', () => {
        req.params.eventId = event1._id;
        return hrRequestsController.getCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert(resRenderSpy.called);
            });
    });


    it('postCreate with valid data', () => {
        req.body = {
            department: DepartmentEnum.SERVICES,
            nbrOfEmployeesNeeded: '123',
            reason: 'Some unique reason'
        };
        req.params.eventId = event1._id;

        return hrRequestsController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return HrRequest.findOne({reason: 'Some unique reason'}).exec()
                    .then(hrRequest => {
                        assert.isNotNull(hrRequest);
                    });
            });
    });

    it('getDetail with valid existing id', () => {
        req.params.requestId = hrRequest1._id;
        return hrRequestsController.getDetails(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.request);
            });
    });

    it('getDetail with valid non-existing id', () => {
        req.params.requestId = '5800e73f1c4bf10b21650407';
        return hrRequestsController.getDetails(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });


    it('getList', () => {
        return hrRequestsController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.requests);
                assert.lengthOf(dataSink.renderData.requests, 2);
            });
    });

    it('getUpdate with valid data', () => {
        req.params.requestId = hrRequest1._id;
        return hrRequestsController.getUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.data);
            });
    });

    it('postUpdate with valid data', () => {
        req.params.requestId = hrRequest1._id;
        req.body = {
            department: DepartmentEnum.SERVICES,
            nbrOfEmployeesNeeded: '123',
            reason: 'Some another unique reason'
        };

        return hrRequestsController.postUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return HrRequest.findOne({_id: hrRequest1._id}).exec()
                    .then(event => {
                        assert.equal(event.reason, 'Some another unique reason');
                    });
            });
    });


    it('getAccept', () => {
        req.params.requestId = hrRequest1._id;
        return hrRequestsController.getAccept(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return HrRequest.findOne({_id: hrRequest1._id}).exec()
                    .then(event => {
                        assert.isTrue(event.archived);
                        assert.isFalse(event.rejected);

                        return HrRequest.update({_id: hrRequest1._id}, {archived: false});
                    });
            });
    });


    it('getReject', () => {
        req.params.requestId = hrRequest1._id;
        return hrRequestsController.getReject(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return HrRequest.findOne({_id: hrRequest1._id}).exec()
                    .then(event => {
                        assert.isTrue(event.archived);
                        assert.isTrue(event.rejected);
                    });
            });
    });



    after('Remove data', () => {
        return Promise.all([HrRequest.remove({}), Event.remove({})]);
    });

});