const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const Client = require('../../../models/Client');
const EventRequest = require('../../../models/EventRequest');
const Event = require('../../../models/Event');

const PreferencesEnum = require('../../../enums/PreferencesEnum');

const eventRequestsController = require('../../../controllers/eventRequests');


describe('[Event Requests Controller]', () => {
    "use strict";

    let client1, eventRequest1;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create data', () => {
        client1 = new Client({
            email: 'test@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        });

        eventRequest1 = new EventRequest({
            eventType: 'Some event',
            dateFrom: new Date(),
            dateTo: new Date(),
            numberOfAttendees: 1234,
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: 1234,
            client: client1._id
        });

        return Promise.all([client1.save(), eventRequest1.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('getCreate', () => {
        return eventRequestsController.getCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert(resRenderSpy.called);
            });
    });


    it('validateForm with valid data', () => {
        req.body = {
            eventType: 'Some event',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '1234',
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: '1234',
            client: client1._id
        };

        const errors = eventRequestsController.validateForm(req);
        assert.isFalse(errors);
    });

    it('validateForm with invalid dates', () => {
        req.body = {
            eventType: 'Some event',
            dateFrom: 'ASD',
            dateTo: '2018-07-AA',
            numberOfAttendees: '1234',
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: '1234',
            client: client1._id
        };

        const errors = eventRequestsController.validateForm(req);

        assert.isArray(errors);
        assert.lengthOf(errors, 7);
    });

    it('validateForm with invalid int values', () => {
        req.body = {
            eventType: 'Some event',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: 'ASD',
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: 'ASD',
            client: client1._id
        };

        const errors = eventRequestsController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 2);
    });

    it('validateForm with invalid MongoId', () => {
        req.body = {
            eventType: 'Some event',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '123',
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: '123',
            client: 'ASD'
        };

        const errors = eventRequestsController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });

    it('validateForm with dateFrom further then dateTo', () => {
        req.body = {
            eventType: 'Some event',
            dateFrom: '2018-07-05',
            dateTo: '2018-07-02',
            numberOfAttendees: '123',
            preferences: [PreferencesEnum.DECORATION, PreferencesEnum.FOOD],
            budget: '123',
            client: client1._id
        };

        const errors = eventRequestsController.validateForm(req);
        assert.isArray(errors);
        assert.lengthOf(errors, 1);
    });

    it('postCreate with valid data', () => {
        req.body = {
            eventType: 'Some great party',
            dateFrom: '2018-07-01',
            dateTo: '2018-07-02',
            numberOfAttendees: '4321',
            preferences: [PreferencesEnum.DECORATION],
            budget: '4321',
            client: client1._id
        };

        return eventRequestsController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return EventRequest.findOne({eventType: 'Some great party'}).exec()
                    .then(eventRequest => {
                        assert.isNotNull(eventRequest);
                        assert.equal(eventRequest.numberOfAttendees, '4321');
                        assert.equal(eventRequest.budget, '4321');
                        assert.isArray(eventRequest.preferences);
                        assert.lengthOf(eventRequest.preferences, 1);
                    });
            });
    });

    it('getList', () => {
        return eventRequestsController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.eventRequests);
                assert.lengthOf(dataSink.renderData.eventRequests, 2);
            });
    });

    it('isIdValid with valid id', () => {
        req.params.eventRequestId = eventRequest1._id;
        const result = eventRequestsController.isIdValid(req, res);

        assert.isTrue(result);
        assert.isFalse(errHandler.called);
        assert.isFalse(resRedirectSpy.called);
        assert.isFalse(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('isIdValid with invalid id', () => {
        req.params.eventRequestId = 'asd';
        const result = eventRequestsController.isIdValid(req, res);

        assert.isFalse(result);
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('isIdValid with no id', () => {
        const result = eventRequestsController.isIdValid(req, res);

        assert.isFalse(result);
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('getDetail with valid existing id', () => {
        req.params.eventRequestId = eventRequest1._id;
        return eventRequestsController.getDetail(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.request);
            });
    });

    it('getDetail with valid non-existing id', () => {
        req.params.eventRequestId = '5800e73f1c4bf10b21650407';
        return eventRequestsController.getDetail(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('getUpdate with valid data', () => {
        req.params.eventRequestId = eventRequest1._id;
        return eventRequestsController.getUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.data);
                assert.isDefined(dataSink.renderData.clients);
            });
    });

    it('postUpdate with valid data', () => {
        req.params.eventRequestId = eventRequest1._id;
        req.body = {
            eventType: 'Some random string',
            dateFrom: '2018-01-01',
            dateTo: '2018-01-02',
            numberOfAttendees: eventRequest1.numberOfAttendees + '', // Have to be string
            preferences: eventRequest1.preferences,
            budget: eventRequest1.budget + '', // Have to be string for validators
            client: eventRequest1.client
        };
        return eventRequestsController.postUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return EventRequest.findOne({_id: eventRequest1._id}).exec()
                    .then(eventRequest => {
                        assert.equal(eventRequest.eventType, 'Some random string');
                    });
            });
    });


    it('getReject', () => {
        req.params.eventRequestId = eventRequest1._id;
        return eventRequestsController.getReject(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return EventRequest.findOne({_id: eventRequest1._id}).exec()
                    .then(eventRequest => {
                        assert.isTrue(eventRequest.rejected);

                        eventRequest.rejected = false;
                        return eventRequest.save();
                    });
            });
    });



    it('getAccept', () => {
        req.params.eventRequestId = eventRequest1._id;
        return eventRequestsController.getAccept(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Event.findOne({eventType: 'Some random string'}).exec()
                    .then(event => {
                        assert.isNotNull(event);
                    });
            });
    });

    after('Remove data', () => {
        return Promise.all([Client.remove({}), EventRequest.remove({}), Event.remove({})]);
    });

});
