const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const ResourceRequest = require('../../../models/ResourceRequest');
const User = require('../../../models/User');
const Task = require('../../../models/Task');
const rrController = require('../../../controllers/resourceRequests');
const DepartmentEnum = require('../../../enums/DepartmentEnum');
const RolesEnum = require('../../../enums/RolesEnum');

describe('ResourceRequest Controller', () => {
    "use strict";

    let requester, task, rr1, rr2;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create resource requests', () => {
        requester = new User({
          name: 'requester',
          email: 'req@sep.se',
          password: 'requester',
          role: 'teamMember'
        });

        task = new Task({
          name: 'Some task'
        });

        rr1 = new ResourceRequest({
            requester: requester._id,
            department: DepartmentEnum.PRODUCTION,
            task: task._id,
            newBudget: 123,
            reason: 'Some reason',
            approvedByDptManager: true
        });

        rr2 = new ResourceRequest({
            requester: requester._id,
            department: DepartmentEnum.PRODUCTION,
            task: task._id,
            newBudget: 456,
            reason: 'Some other reason',
            archived: true
        });

        return Promise.all([requester.save(), task.save(), rr1.save(), rr2.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('getCreate with invalid id', () => {
        req.params.taskId = 'abc123';
        rrController.getCreate(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('getCreate with valid id', () => {
        req.params.taskId = task._id;
        return rrController.getCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);
            });
    });

    it('getList sent by someone who should not see the request', () => {
        req.user.role = RolesEnum.SERVICE_MANAGER;
        req.user._id = requester._id;
        return rrController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.requests);
                assert.lengthOf(dataSink.renderData.requests, 0);
            });
    });

    it('getList sent by a person who should see the request with archived', () => {
        req.user.role = RolesEnum.PRODUCTION_MANAGER;
        req.query.archived = true;
        return rrController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.requests);
                assert.lengthOf(dataSink.renderData.requests, 2);
            });
    });

    it('getList sent by a person who should see the request without archived', () => {
        req.user.role = RolesEnum.PRODUCTION_MANAGER;
        return rrController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.requests);
                assert.lengthOf(dataSink.renderData.requests, 1);
            });
    });

    it('getUpdate with invalid id', () => {
        req.params.requestId = 'abc123';
        rrController.getUpdate(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('getUpdate with valid id', () => {
        req.params.requestId = rr1._id;
        rrController.getUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);
            });
    });

    it('getAccept with invalid id', () => {
        req.params.requestId = 'abc123';
        rrController.getAccept(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('getAccept with valid id but for archived request', () => {
        req.params.requestId = rr2._id;
        rrController.getAccept(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

  it('getAccept with valid id and for unarchived request', () => {
        req.params.requestId = rr1._id;
        rrController.getAccept(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('getDetails with invalid id', () => {
        req.params.requestId = 'abc123';
        rrController.getDetails(req, res, errHandler);
        
        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);

        assert.isUndefined(dataSink.renderData);
    });

    it('getDetails with valid id', () => {
        req.params.requestId = rr1._id;
        rrController.getDetails(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.request);
                assert.equal(dataSink.renderData.request.requester, requester._id);
                assert.isTrue(dataSink.renderData.request.approvedByDptManager);
                assert.isTrue(dataSink.renderData.request.archived);
                assert.isFalse(dataSink.renderData.request.rejected);
            });
    });

    it('postCreate with invalid data', () => {
        req.params.taskId = task._id;
        req.body.department = '';
        rrController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.data);
                assert.equal(dataSink.renderData.data.department, '');
            });
    });

    it('postCreate with valid data', () => {
        req.params.taskId = task._id;
        req.body.department = DepartmentEnum.PRODUCTION;
        req.body.newBudget = '123';
        req.body.reason = 'Some new reason';
        rrController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('postUpdate with invalid data', () => {
        req.params.requestId = rr1._id;
        req.body.department = '';
        rrController.postUpdate(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.called);
        assert.isFalse(resRenderSpy.called);
    });

    it('postUpdate with valid data', () => {
        req.params.requestId = rr1._id;
        req.body.department = DepartmentEnum.PRODUCTION;
        req.body.newBudget = '123';
        req.body.reason = 'Some new reason';
        rrController.postUpdate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    after('Clean up user and task', () => {
        "use strict";
        return Promise.all([requester.remove(), task.remove()]);
    });

    after('Remove clients', () => {
        return ResourceRequest.remove({});
    });

});
