const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const Client = require('../../../models/Client');
const Task = require('../../../models/Task');
const Event = require('../../../models/Event');
const User = require('../../../models/User');

const PreferencesEnum = require('../../../enums/PreferencesEnum');
const PrioritiesEnum = require('../../../enums/PrioritiesEnum');

const tasksController = require('../../../controllers/tasks');


describe('[Tasks Controller]', () => {
    "use strict";


    let client1, event1, task1, task2, user1;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create data', () => {
        client1 = new Client({
            email: 'test@gmail.com',
            name: 'Client2',
            phoneNumber: '3456789'
        });

        user1 = new User({
            email: 'test@gmail.com',
            password: 'password',
            role: 'admin'
        });

        event1 = new Event({
            eventType: 'Some event',
            description: 'Some event description',
            dateFrom: new Date(),
            dateTo: new Date(),
            numberOfAttendees: 1234,
            tasks: [],
            budget: 1234,
            client: client1._id
        });

        task1 = new Task({
            name: PreferencesEnum.DECORATION,
            description: 'Some description for decoration task',
            budget: 1234,
            event: event1._id,
            assignedTo: user1._id,
            assignedBy: user1._id,
            priority: PrioritiesEnum.HIGH
        });

        task2 = new Task({
            name: PreferencesEnum.FOOD,
            description: 'Some description for food task',
            budget: 1234,
            event: event1._id,
            assignedTo: user1._id,
            assignedBy: user1._id,
            priority: PrioritiesEnum.HIGH
        });


        return Promise.all([client1.save(), event1.save(), task1.save(), task2.save(), user1.save()]);
    });

    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('getList not completed', () => {
        req.user._id = user1._id;
        return tasksController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.tasks);
                assert.lengthOf(dataSink.renderData.tasks, 2);
            });
    });

    it('getComplete with invalid id', () => {
        req.params.taskId = 'asd';
        tasksController.getComplete(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isTrue(resRedirectSpy.called);
        assert.isTrue(reqFlashSpy.calledWith('errors'));
        assert.isFalse(resRenderSpy.called);
    });

    it('getComplete with not existing id', () => {
        req.params.taskId = '5800e66e7cd2760b10a2cfe8';
        return tasksController.getComplete(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('getComplete with valid existing id but with user not assigned to task', () => {
        req.params.taskId = task1._id;
        return tasksController.getComplete(req, res, errHandler)
            .then(() => {
                assert.isTrue(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('error'));
                assert.isFalse(resRenderSpy.called);
            });
    });

    it('getComplete with valid data', () => {
        req.params.taskId = task1._id;
        req.user._id = user1._id;
        return tasksController.getComplete(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return Task.findOne({_id: task1._id}).exec()
                    .then(task => {
                        assert.isNotNull(task);
                        assert.isTrue(task.completed);
                    });
            });
    });


    it('getList not completed - again', () => {
        req.user._id = user1._id;
        return tasksController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.tasks);
                assert.lengthOf(dataSink.renderData.tasks, 1);
            });
    });

    after('Remove data', () => {
        return Promise.all([Client.remove({}), Task.remove({}), User.remove({}), Event.remove({})]);
    });

});
