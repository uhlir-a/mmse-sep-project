const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const homeController = require('../../../controllers/home');


describe('[Home Controller]', () => {
    "use strict";

    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('index', () => {
        homeController.index(req, res, errHandler);

        assert.isFalse(errHandler.called);
        assert.isFalse(resRedirectSpy.called);
        assert.isFalse(reqFlashSpy.called);
        assert.isTrue(resRenderSpy.called);
    });

});