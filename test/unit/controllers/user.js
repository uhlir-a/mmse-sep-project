const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;
const createRequest = require('../tools/request');
const createRespond = require('../tools/respond');

// Test specific imports
const User = require('../../../models/User');
const RolesEnum = require('../../../enums/RolesEnum');
const userController = require('../../../controllers/user');


describe('[Users Controller]', () => {
    "use strict";

    let user1;
    let dataSink, req, res, errHandler;
    let reqFlashSpy, resRenderSpy, resRedirectSpy;

    before('Create data', () => {
        user1 = new User({
            email: 'test@gmail.com',
            password: 'password',
            role: 'admin'
        });

        return Promise.all([user1.save()]);
    });


    beforeEach('Init request and respond objects', () => {
        dataSink = {};
        res = createRespond(dataSink);

        resRenderSpy = sinon.spy(res, 'render');
        resRedirectSpy = sinon.spy(res, 'redirect');

        errHandler = sinon.spy();

        return createRequest().then(newReq => {
            req = newReq;
            reqFlashSpy = sinon.spy(req, 'flash');
        });
    });

    it('getList', () => {
        return userController.getList(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isFalse(resRedirectSpy.called);
                assert.isFalse(reqFlashSpy.called);
                assert.isTrue(resRenderSpy.called);

                assert.isDefined(dataSink.renderData.users);
                assert.lengthOf(dataSink.renderData.users, 1);
            });
    });


    it('getCreate', () => {
        userController.getCreate(req, res, errHandler);
        
        assert.isFalse(errHandler.called);
        assert.isFalse(resRedirectSpy.called);
        assert.isFalse(reqFlashSpy.called);
        assert.isTrue(resRenderSpy.called);
    });

    it('postCreate with valid data', () => {
        req.body = {
            email: 'email@email.cz',
            name: 'Some name',
            role: RolesEnum.ADMINISTRATION_MANAGER,
            password: 'password',
            confirmPassword: 'password'
        };

        return userController.postCreate(req, res, errHandler)
            .then(() => {
                assert.isFalse(errHandler.called);
                assert.isTrue(resRedirectSpy.called);
                assert.isTrue(reqFlashSpy.calledWith('success'));
                assert.isFalse(resRenderSpy.called);

                return User.findOne({email: 'email@email.cz'}).exec()
                    .then(user => {
                        assert.isNotNull(user);
                    });
            });
    });

    after('Remove data', () => {
        return Promise.all([User.remove({})]);
    });

});