
module.exports = (extractData) => {
    return {
        render: (path, data) => {extractData.redirectPath = path; extractData.renderData = data;},
        redirect: (path) => {extractData.redirectPath = path;}
    };
};