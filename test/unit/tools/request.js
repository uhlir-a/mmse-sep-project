expressValidator = require('express-validator')();

module.exports = (body = {}, params = {}, query = {}) => {
    let req = {
        query: query,
        body: body,
        params: params,
        user: {
            _id: '5800e66e7cd2760b10a2cfe8',
            role: 'admin'
        },
        param: (name) => this.params[name],
        flash: () => {}
    };

    return new Promise((accept, reject) => {
        expressValidator(req, {}, () => {
            accept(req);
        });
    });
};