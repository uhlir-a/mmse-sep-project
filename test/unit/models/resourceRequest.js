const chai = require('chai');
const expect = chai.expect;
const ResourceRequest = require('../../../models/ResourceRequest');
const User = require('../../../models/User');
const Task = require('../../../models/Task');
const DepartmentEnum = require('../../../enums/DepartmentEnum');

describe('ResourceRequest Model', () => {
  let requester, task;

  before('create User and Task', () => {
    "use strict";

    requester = new User({
      name: 'Some user name',
      email: 'user@sep.se'
    });

    task = new Task({
      name: 'Some task name'
    });

    return Promise.all([requester.save(), task.save()]);
  });

  after('clean up User and Task', () => {
    "use strict";
    return Promise.all([requester.remove(), task.remove()]);
  });

  it('should create a new ResourceRequest', (done) => {
    const resourceRequest = new ResourceRequest({
      requester: requester._id,
      department: DepartmentEnum.PRODUCTION,
      task: task._id,
      newBudget: 345,
      reason: 'Some reason'
    });

    resourceRequest.save((err) => {
      expect(err).to.be.null;
      expect(resourceRequest).to.have.property('requester');
      expect(resourceRequest.requester).to.equal(requester._id);
      expect(resourceRequest).to.have.property('department');
      expect(resourceRequest.department).to.equal(DepartmentEnum.PRODUCTION);
      expect(resourceRequest).to.have.property('task');
      expect(resourceRequest.task).to.equal(task._id);
      expect(resourceRequest).to.have.property('newBudget');
      expect(resourceRequest.newBudget).to.equal(345);
      expect(resourceRequest).to.have.property('reason');
      expect(resourceRequest.reason).to.equal('Some reason');
      expect(resourceRequest).to.have.property('archived');
      expect(resourceRequest.archived).to.equal(false);
      expect(resourceRequest).to.have.property('approvedByDptManager');
      expect(resourceRequest.approvedByDptManager).to.equal(false);
      expect(resourceRequest).to.have.property('rejected');
      expect(resourceRequest.rejected).to.equal(false);
      done();
    });
  });

  it('should find ResourceRequest by requester id', (done) => {
    ResourceRequest.findOne({ requester: requester._id }, (err, resourceRequest) => {
      expect(err).to.be.null;
      expect(resourceRequest.newBudget).to.equal(345);
      done();
    });
  });

  it('should delete an ResourceRequest', (done) => {
    ResourceRequest.remove({ requester: requester._id }, (err) => {
      expect(err).to.be.null;
      done();
    });
  });

  after('Clean up the data', () => {
    return ResourceRequest.remove({});
  });
});
