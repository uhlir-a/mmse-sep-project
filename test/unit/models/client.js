const chai = require('chai');
const expect = chai.expect;
const Client = require('../../../models/Client');

describe('Client Model', () => {
  it('should create a new Client', (done) => {
    const client = new Client({
      email: 'test@dev.com',
      name: 'name',
      phoneNumber: '3456789'
    });
    client.save((err) => {
      expect(err).to.be.null;
      expect(client.email).to.equal('test@dev.com');
      expect(client).to.have.property('name');
      expect(client).to.have.property('phoneNumber');
      done();
    });
  });

  it('should not create a client without the unique email', (done) => {
    const client = new Client({
      email: 'test@dev.com',
      name: 'name',
      phoneNumber: '3456789'
    });
    client.save((err) => {
      expect(err).to.be.defined;
      expect(err.code).to.equal(11000);
      done();
    });
  });

  it('should find client by email', (done) => {
    Client.findOne({ email: 'test@dev.com' }, (err, client) => {
      expect(err).to.be.null;
      expect(client.email).to.equal('test@dev.com');
      done();
    });
  });

  it('should delete a client', (done) => {
    Client.remove({ email: 'test@dev.com' }, (err) => {
      expect(err).to.be.null;
      done();
    });
  });

  after('Clean up the data', () => {
    return Client.remove({});
  });
});
