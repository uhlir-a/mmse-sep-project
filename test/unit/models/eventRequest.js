const chai = require('chai');
const expect = chai.expect;
const EventRequest = require('../../../models/EventRequest');
const Client = require('../../../models/Client');
const User = require('../../../models/User');

describe('EventRequest Model', () => {
  let client, reviewer, createdBy;

  before('create Client and Users', () => {
    "use strict";

    client = new Client({
      name: 'Some name'
    });

    reviewer = new User({
      name: 'User1',
      email: 'user1@sep.se'
    });

    createdBy = new User({
      name: 'User2',
      email: 'user2@sep.se'
    });

    return Promise.all([client.save(), reviewer.save(), createdBy.save()]);
  });

  after('clean up Client and Users', () => {
    "use strict";
    return Promise.all([client.remove(), reviewer.remove(), createdBy.remove()]);
  });

  it('should create a new EventRequest', (done) => {
    const eventRequest = new EventRequest({
      eventType: 'Some event request type',
      dateFrom: new Date(),
      dateTo: new Date(),
      numberOfAttendees: 789,
      preferences: ['Preference1', 'Preference2'],
      budget: 456,
      client: client._id,
      reviews: [{
        user: reviewer._id,
        feedback: 'Some feedback'
      }],
      createdBy: createdBy._id
    });

    eventRequest.save((err) => {
      expect(err).to.be.null;
      expect(eventRequest).to.have.property('eventType');
      expect(eventRequest.eventType).to.equal('Some event request type');
      expect(eventRequest).to.have.property('dateFrom');
      expect(eventRequest.dateFrom).to.not.be.null;
      expect(eventRequest).to.have.property('numberOfAttendees');
      expect(eventRequest.numberOfAttendees).to.be.a('number');
      expect(eventRequest.numberOfAttendees).to.equal(789);
      expect(eventRequest).to.have.property('client');
      expect(eventRequest.client).to.be.an('object');
      expect(eventRequest.client).to.equal(client._id);
      expect(eventRequest).to.have.property('reviews');
      expect(eventRequest.reviews).to.an('array');
      expect(eventRequest).to.have.property('rejected');
      expect(eventRequest.rejected).to.equal(false);
      done();
    });
  });

  it('should find event request by client id', (done) => {
    EventRequest.findOne({ client: client._id }, (err, eventRequest) => {
      expect(err).to.be.null;
      expect(eventRequest.numberOfAttendees).to.equal(789);
      done();
    });
  });

  it('should delete an event request', (done) => {
    EventRequest.remove({ _id: client._id }, (err) => {
      expect(err).to.be.null;
      done();
    });
  });

  after('Clean up the data', () => {
    return EventRequest.remove({});
  });
});
