const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const Event = require('../../../models/Event');
const Task = require('../../../models/Task');
const Client = require('../../../models/Client');

describe('Event Model', () => {
  let task, client;

  before('create Task and Client', () => {
    "use strict";

    task = Task({
      name: 'Decoration'
    });

    client = new Client({
      email: 'test@gmail.com',
      name: 'name',
      phoneNumber: '3456789'
    });

    return Promise.all([task.save(), client.save()]);
  });

  after('clean up of Task and Client', () =>{
    "use strict";

    return Promise.all([task.remove(), client.remove()]);
  });

  it('should create a new Event', (done) => {
    const event = new Event({
      eventType: 'Some event',
      dateFrom: new Date(),
      budget: 1234,
      dateTo: new Date(),
      numberOfAttendees: 321,
      tasks: [task._id],
      client: client._id,
      description: "Some description"
    });
    event.save((err) => {
      expect(err).to.be.null;
      expect(event.eventType).to.equal('Some event');
      expect(event.description).to.equal('Some description');
      expect(event.budget).to.equal(1234);
      expect(event.archived).to.be.false;
      expect(event).to.have.property('dateFrom');
      done();
    });
  });

  it('should find all events according budget', () => {
    return Event.find({ budget: 1234 }).populate('tasks client').exec()
        .then(events => {
          "use strict";
          expect(events).to.be.a('array');
          expect(events).to.have.lengthOf(1);
          let event = events[0];
          expect(event.eventType).to.equal('Some event');
          expect(event.description).to.equal('Some description');

          expect(event.tasks).to.be.a('array');
          expect(event.tasks).to.have.lengthOf(1);
          expect(event.tasks[0].name).to.equal('Decoration');

          expect(event.client).to.be.a('object');
          expect(event.client.email).to.equal('test@gmail.com');

        });
  });

  after('should delete a event', () => {
      return Promise.all([Event.remove({}), Client.remove({})]);
  });
});
