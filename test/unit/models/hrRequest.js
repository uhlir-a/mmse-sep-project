const chai = require('chai');
const expect = chai.expect;
const HrRequest = require('../../../models/HrRequest');
const Event = require('../../../models/Event');
const User = require('../../../models/User');
const DepartmentEnum = require('../../../enums/DepartmentEnum');

describe('HrRequest Model', () => {
  let event;

  before('create Event', () => {
    "use strict";

    event = new Event({
      eventType: 'Some event type'
    });

    return Promise.all([event.save()]);
  });

  after('clean up Event', () => {
    "use strict";
    return Promise.all([event.remove()]);
  });

  it('should create a new HrRequest', (done) => {
    const hrRequest = new HrRequest({
      department: DepartmentEnum.PRODUCTION,
      nbrOfEmployeesNeeded: 5,
      reason: 'Some reason',
      event: event._id
    });

    hrRequest.save((err) => {
      expect(err).to.be.null;
      expect(hrRequest).to.have.property('department');
      expect(hrRequest.department).to.equal(DepartmentEnum.PRODUCTION);
      expect(hrRequest).to.have.property('nbrOfEmployeesNeeded');
      expect(hrRequest.nbrOfEmployeesNeeded).to.equal(5);
      expect(hrRequest).to.have.property('reason');
      expect(hrRequest.reason).to.equal('Some reason');
      expect(hrRequest).to.have.property('event');
      expect(hrRequest.event).to.equal(event._id);
      expect(hrRequest).to.have.property('archived');
      expect(hrRequest.archived).to.equal(false);
      expect(hrRequest).to.have.property('rejected');
      expect(hrRequest.rejected).to.equal(false);
      done();
    });
  });

  it('should find HrRequest by event id', (done) => {
    HrRequest.findOne({ event: event._id }, (err, hrRequest) => {
      expect(err).to.be.null;
      expect(hrRequest.reason).to.equal('Some reason');
      done();
    });
  });

  it('should delete an HrRequest', (done) => {
    HrRequest.remove({ event: event._id }, (err) => {
      expect(err).to.be.null;
      done();
    });
  });

  after('Clean up the data', () => {
    return HrRequest.remove({});
  });
});
