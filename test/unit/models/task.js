const chai = require('chai');
const expect = chai.expect;
const Task = require('../../../models/Task');
const Event = require('../../../models/Event');
const User = require('../../../models/User');
const PrioritiesEnum = require('../../../enums/PrioritiesEnum');

describe('Task Model', () => {
  let event, assignedTo, assignedBy;

  before('create Event and Users', () => {
    "use strict";

    event = new Event({
      eventType: 'Some event type',
      description: 'Some event description'
    });

    assignedTo = new User({
      name: 'User1',
      email: 'user1@sep.se'
    });

    assignedBy = new User({
      name: 'User2',
      email: 'user2@sep.se'
    });

    return Promise.all([event.save(), assignedTo.save(), assignedBy.save()]);
  });

  after('clean up Event and Users', () => {
    "use strict";
    return Promise.all([event.remove(), assignedTo.remove(), assignedBy.remove()]);
  });

  it('should create a new Task', (done) => {
    const task = new Task({
      name: 'Some name',
      event: event._id,
      description: 'Some task description',
      budget: 123,
      assignedTo: assignedTo._id,
      assignedBy: assignedBy._id,
      priority: PrioritiesEnum.MIDDLE
    });

    task.save((err) => {
      expect(err).to.be.null;
      expect(task).to.have.property('name');
      expect(task.name).to.equal('Some name');
      expect(task).to.have.property('event');
      expect(task.event).to.equal(event._id);
      expect(task).to.have.property('description');
      expect(task.description).to.equal('Some task description');
      expect(task).to.have.property('budget');
      expect(task.budget).to.equal(123);
      expect(task).to.have.property('assignedTo');
      expect(task.assignedTo).to.equal(assignedTo._id);
      expect(task).to.have.property('assignedBy');
      expect(task.assignedBy).to.equal(assignedBy._id);
      expect(task).to.have.property('priority');
      expect(task.priority).to.equal(PrioritiesEnum.MIDDLE);
      expect(task).to.have.property('completed');
      expect(task.completed).to.equal(false);
      done();
    });
  });

  it('should find task by name', (done) => {
    Task.findOne({ name: 'Some name' }, (err, task) => {
      expect(err).to.be.null;
      expect(task.name).to.equal('Some name');
      done();
    });
  });

  it('should delete a task', (done) => {
    Task.remove({ name: 'Some name' }, (err) => {
      expect(err).to.be.null;
      done();
    });
  });

  after('Clean up the data', () => {
    return Task.remove({});
  });
});
