/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const dotenv = require('dotenv');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const expressValidator = require('express-validator');
const expressStatusMonitor = require('express-status-monitor');
const multer = require('multer');
const upload = multer({dest: path.join(__dirname, 'uploads')});
const nunjucks = require('nunjucks');
const dateFilter = require('nunjucks-date-filter');
const Roles = require('./tools/roles');
const validators = require('./tools/validators');
const sanitizers = require('./tools/sanitizors');
const RedirectError = require('./tools/RedirectError');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({path: '.env'});

/**
 * Controllers (route handlers).
 */
const homeController = require('./controllers/home');
const userController = require('./controllers/user');
const clientController = require('./controllers/client');
const eventRequestsController = require('./controllers/eventRequests');
const eventsController = require('./controllers/events');
const tasksController = require('./controllers/tasks');
const resourceRequestsController = require('./controllers/resourceRequests');
const hrRequestsController = require('./controllers/hrRequests');

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
const app = express();

/**
 * Connect to MongoDB.
 */
mongoose.Promise = global.Promise;
mongoose.connect(process.env.NODE_ENV == 'test' ? process.env.MONGODB_TEST_URI : process.env.MONGODB_URI);
mongoose.connection.on('connected', () => {
    console.log('%s MongoDB connection established!', chalk.green('✓'));
});
mongoose.connection.on('error', () => {
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});

/**
 * Roles
 */
const roles = require('./enums/RolesEnum');
const unathorizedCb = (req, res) => {
    "use strict";

    req.flash('error', {msg: 'You do not have sufficient rights to access this page!'});
    res.redirect('/');
};
const notLoggedInCb = (req, res) => {
    "use strict";

    req.flash('error', {msg: 'To access this page, you have to be logged in!'});
    res.redirect('/login');
};
const user = new Roles(roles._inheritance, notLoggedInCb, unathorizedCb);
app.use(user.middleware());

/**
 * Nunjucks templating engine configuration
 */
const nunInstance = nunjucks.configure('views', {
    noCache: true,
    autoescape: true,
    express: app
});
nunInstance.addGlobal('roles', roles).addFilter('hasRole', (userInstance, role) => {
    "use strict";
    return user.checkUserRole(userInstance.role, role);
});
nunInstance.addFilter('date', dateFilter);
dateFilter.setDefaultFormat('D.M.YYYY');

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 8000);
app.set('views', path.join(__dirname, 'views'));
app.use(expressStatusMonitor());
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator({
    customValidators: validators,
    customSanitizers: sanitizers
}));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.SESSION_SECRET,
    store: new MongoStore({
        url: process.env.NODE_ENV == 'test' ? process.env.MONGODB_TEST_URI : process.env.MONGODB_URI,
        autoReconnect: true
    })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use((req, res, next) => {
    res.locals.user = req.user;
    res.locals.url = req.url;
    next();
});
app.use(function (req, res, next) {
    // After successful login, redirect back to the intended page
    if (!req.user &&
        req.path !== '/login' &&
        req.path !== '/signup' && !req.path.match(/\./)) {
        req.session.returnTo = req.path;
    }
    next();
});
app.use(express.static(path.join(__dirname, 'public', 'build'), {maxAge: 31557600000}));

/**
 * Primary app routes.
 */
app.get('/', passportConfig.isAuthenticated, homeController.index);
app.get('/login', userController.getLogin);
app.post('/login', userController.postLogin);
app.get('/logout', userController.logout);

// TODO: Add Role check + for POST /users/create add dependency on NODE_ENV for Tests
app.get('/users/', userController.getList);
app.get('/users/create',  userController.getCreate);
app.post('/users/create', userController.postCreate);

// Clients
app.get('/clients/create', user.hasRole([roles.CUSTOMER_SERVICE]), clientController.getCreate);
app.post('/clients/create', user.hasRole([roles.CUSTOMER_SERVICE]), clientController.postCreate);
app.get('/clients',  user.hasRole([roles.CUSTOMER_SERVICE, roles.ADMINISTRATION_TEAM]), clientController.getList);
app.get('/clients/:clientId/update', user.hasRole([roles.CUSTOMER_SERVICE]), clientController.getUpdate);
app.post('/clients/:clientId/update', user.hasRole([roles.CUSTOMER_SERVICE]), clientController.postUpdate);
app.get('/clients/:clientId', user.hasRole([roles.CUSTOMER_SERVICE, roles.ADMINISTRATION_TEAM]), clientController.getEventHistory);

// Event Requests
app.get('/event-requests/create', user.hasRole([roles.CUSTOMER_SERVICE]), eventRequestsController.getCreate);
app.post('/event-requests/create', user.hasRole([roles.CUSTOMER_SERVICE]),eventRequestsController.postCreate);
app.get('/event-requests', user.hasRole([roles.CUSTOMER_SERVICE]), eventRequestsController.getList);
app.get('/event-requests/:eventRequestId/update', user.hasRole([roles.CUSTOMER_SERVICE]), eventRequestsController.getUpdate);
app.post('/event-requests/:eventRequestId/update', user.hasRole([roles.CUSTOMER_SERVICE]), eventRequestsController.postUpdate);
app.get('/event-requests/:eventRequestId/review',  user.hasRole([roles.SENIOR_CUSTOMER_SERVICE, roles.FINANCIAL_MANAGER]), eventRequestsController.getReview);
app.post('/event-requests/:eventRequestId/review', user.hasRole([roles.SENIOR_CUSTOMER_SERVICE, roles.FINANCIAL_MANAGER]), eventRequestsController.postReview);
app.get('/event-requests/:eventRequestId/reject', user.hasRole([roles.SENIOR_CUSTOMER_SERVICE, roles.ADMINISTRATION_MANAGER]), eventRequestsController.getReject);
app.get('/event-requests/:eventRequestId/accept', user.hasRole([roles.ADMINISTRATION_MANAGER]), eventRequestsController.getAccept);
app.get('/event-requests/:eventRequestId', user.hasRole([roles.CUSTOMER_SERVICE, roles.ADMINISTRATION_MANAGER]), eventRequestsController.getDetail);

// Events
app.get('/events', passportConfig.isAuthenticated, eventsController.getList);
app.get('/events/:eventId', passportConfig.isAuthenticated, eventsController.getDetail);
app.post('/events/:eventId', user.hasRole([roles.DEPARTMENT_MANAGER]), eventsController.postAssignTask);
app.get('/events/:eventId/update', user.hasRole([roles.ADMINISTRATION_TEAM, roles.SENIOR_CUSTOMER_SERVICE, roles.DEPARTMENT_MANAGER]), eventsController.getUpdate);
app.post('/events/:eventId/update', user.hasRole([roles.ADMINISTRATION_TEAM, roles.SENIOR_CUSTOMER_SERVICE, roles.DEPARTMENT_MANAGER]), eventsController.postUpdate);
app.get('/events/:eventId/archive', user.hasRole([roles.ADMINISTRATION_TEAM]), eventsController.getArchive);

// Tasks
app.get('/tasks', user.hasRole([roles.TEAM_MEMBER]), tasksController.getList);
app.get('/tasks/:taskId/complete', user.hasRole([roles.TEAM_MEMBER]), tasksController.getComplete);


// Resource Requests
app.get('/resource-requests/:taskId/create', user.hasRole([roles.TEAM_MEMBER]), resourceRequestsController.getCreate);
app.post('/resource-requests/:taskId/create', user.hasRole([roles.TEAM_MEMBER]), resourceRequestsController.postCreate);
app.get('/resource-requests/:requestId/update', user.hasRole([roles.TEAM_MEMBER]), resourceRequestsController.getUpdate);
app.post('/resource-requests/:requestId/update', user.hasRole([roles.TEAM_MEMBER]), resourceRequestsController.postUpdate);
app.get('/resource-requests/:requestId/approve', user.hasRole([roles.DEPARTMENT_MANAGER]), resourceRequestsController.getApprove);
app.get('/resource-requests/:requestId/accept', user.hasRole([roles.FINANCIAL_MANAGER]), resourceRequestsController.getAccept);
app.get('/resource-requests/:requestId/reject', user.hasRole([roles.FINANCIAL_MANAGER, roles.DEPARTMENT_MANAGER]), resourceRequestsController.getReject);
app.get('/resource-requests/:requestId', user.hasRole([roles.DEPARTMENT_MANAGER, roles.FINANCIAL_MANAGER, roles.TEAM_MEMBER]), resourceRequestsController.getDetails);
app.get('/resource-requests', user.hasRole([roles.DEPARTMENT_MANAGER, roles.FINANCIAL_MANAGER, roles.TEAM_MEMBER]), resourceRequestsController.getList);

// HR Requests
app.get('/hr-requests/:eventId/create', user.hasRole([roles.DEPARTMENT_MANAGER]), hrRequestsController.getCreate);
app.post('/hr-requests/:eventId/create', user.hasRole([roles.DEPARTMENT_MANAGER]), hrRequestsController.postCreate);
app.get('/hr-requests/:requestId/update', user.hasRole([roles.DEPARTMENT_MANAGER]), hrRequestsController.getUpdate);
app.post('/hr-requests/:requestId/update', user.hasRole([roles.DEPARTMENT_MANAGER]), hrRequestsController.postUpdate);
app.get('/hr-requests/:requestId/accept', user.hasRole([roles.HR_TEAM]), hrRequestsController.getAccept);
app.get('/hr-requests/:requestId/reject', user.hasRole([roles.HR_TEAM]), hrRequestsController.getReject);
app.get('/hr-requests/:requestId', user.hasRole([roles.HR_TEAM, roles.DEPARTMENT_MANAGER]), hrRequestsController.getDetails);
app.get('/hr-requests', user.hasRole([roles.HR_TEAM, roles.DEPARTMENT_MANAGER]), hrRequestsController.getList);

/**
 * Error Handler.
 */
const errorHandlerFunction = errorHandler();
app.use((err, req, res, next) => {
    "use strict";

    if(err instanceof RedirectError){
        res.redirect(err.getUrl());
    }else{
        errorHandlerFunction(err, req, res, next);
    }
});

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    console.log('%s Express server listening on port %d in %s mode.', chalk.green('✓'), app.get('port'), app.get('env'));
});

module.exports = app;