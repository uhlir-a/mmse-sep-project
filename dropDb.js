const dotenv = require('dotenv');
const mongoose = require('mongoose');
const chalk = require('chalk');

dotenv.load({path: '.env'});
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
mongoose.connection.on('connected', () => {
    console.log('%s MongoDB connection established!', chalk.green('✓'));

    mongoose.connection.db.dropDatabase(() => {
        "use strict";
        console.log('%s MongoDB database dropped!', chalk.green('✓'));
		process.exit();
    });

});
mongoose.connection.on('error', () => {
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});