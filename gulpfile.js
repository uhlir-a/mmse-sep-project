var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

var mainBowerFiles = require('main-bower-files');

var imageTypes = ['jpg', 'jpeg', 'png', 'gif'].join(',');

var lessOptions = {
};

gulp.task('js:libs', function () {
    return gulp.src(mainBowerFiles('**/*.js'))
        .pipe(plugins.concat('libs.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest('public/build/'))
        .pipe(plugins.notify('JS:libs build finished!'));
});

gulp.task('js:src', function () {
    return gulp.src(['public/js/**/*.js'])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.concat('main.js'))
        .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('public/build/'))
        .pipe(plugins.notify('JS build finished!'));
});

gulp.task('css', function () {
    return gulp.src('public/css/main.less')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.less(lessOptions)
            .on('error', plugins.notify.onError("CSS build failed!"))
        )
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.addSrc.prepend(mainBowerFiles('**/*.css')))
        .pipe(plugins.concat('main.css'))
        .pipe(gulp.dest('public/build/'))
        .pipe(plugins.notify('CSS build finished!'));
});

gulp.task('img', function () {

    return gulp.src(mainBowerFiles('**/*.{' + imageTypes + '}'))
        .pipe(plugins.addSrc('public/img/**/*.{' + imageTypes + '}'))
        .pipe(gulp.dest('public/build/img/'));
});

gulp.task('build', ['js:libs', 'js:src', 'css', 'img']);


gulp.task('default', ['build'], function(){
    gulp.watch('public/css/**/*', ['css']);
    gulp.watch('public/js/**/*', ['js:src']);
    gulp.watch('public/img/**/*.{' + imageTypes + '}', ['img']);
    gulp.watch('bower.json', ['build']);
});