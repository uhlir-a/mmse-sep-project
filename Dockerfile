FROM node:6.7.0-onbuild

RUN ./node_modules/.bin/bower install --allow-root
RUN chmod +x run.sh

EXPOSE 8000